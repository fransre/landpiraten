---
source: Enkairoi
created: '2011-02-12'
author: Jürgen
title: Mind
categories:
---


Definition of Mind by Daniel Siegel:
- “A core aspect of the mind can be defined as an embodied and relational process that regulates the flow of energy and information.”

One thing to note is that this definition means we must separate “mind” from “awareness”…

The triangle of well-being and resilience:
- brain
   - nervous system throughout the body
- relationships
- mind
   - “a core aspect of the mind can be defined as an embodied and relational process that regulates the flow of energy and information.”

***
Mental health:
- usual definitions:
   - (worst case) absence of symptoms
   - (best case but it is descriptive) positive psychology
   - philosophers of mind: it is fundamentally impossible (scientifically unsound) to define the mind
- definition of mind by Daniel Siegel:
- “a core aspect of the mind can be defined as an embodied and relational process that regulates the flow of energy and information.”

Mindsight is the way we can focus attention to the nature of the internal world and also on the internal world of someone else. It is how we have insight into ourselves and have empathy for somebody else. Mindsight gives us the capacity to see the mind and to shape it toward health. Empowering action oriented way of describing the power of the mind to actually change the structure and function the brain.

Psychotherapy: help people to strengthen the mind\
If the mind is a regulatory process it has to involve two fundamental processes: monitoring/perception and modifying.\
In helping the people to develop their minds, it is useful to
- have people being able to monitor their internal world, thoughts, feelings, behavioral impulses, perceptions, memories,
- to be able to improve the capacity to sense that
- have people being able to modify the energy and information flow

Integration: linkage of differentiated parts,\
otherwise you have rigidity or chaos.
- integration of conscienciousness
- bilateral integration
- vertical integration
- memory integration
- narrative integration
- state integration
- interpersonal integration
- temporal integration
- transpirational integration

When a system becomes integrated amazingly it moves to an harmonious flow.

A good relationship: harmonious uncertainty.

***
Basis R's of education:
- Reading
- wRiting
- aRithmatic

need to be extended with:
- Reflection
- Relationships
- Resilience

Our experiences shape the brain. The brain has two very different sets of circuits. Circuits about the physical world and about the world of the mind. When we see the mind, the internal life of ourselves and of others, through insight and empathy that can be called mindsight. And there is a power behind developing these circuits that basically never happens in schools. It is unbelievable opportunity to transform not only a child's life, a family's life and a community's life but even to awaken people to the fact that we are all interconnected. The brain left to its own devices in the modern culture comes up with, what Einstein called, an optical delusion of our separateness. And if we get this reflection into the internal world going we can desolve this delusion.

| **Brain stem functions** |	**Limbic area functions** |	**Middle prefrontal functions** |	**Posterior functions**
| heart rate |	appraisal of significance of events |	bodily regulation |	processing of external signals as vision, sound, touch
| respiration |	motivational states |	attuned communication |	(the more forward you get the more abstract the processing gets)
| nucelei for fight, flight, freeze response |	different kinds of memory systems |	emotional balance |	
| | 	affective states, emotions |	fear extinction	|
| | 	attachment relationships |	response flexibility	|
| | | 	 	insight	 |
| | | 	 	empathy	 |
| | | 	 	morality	|
| | | 	 	intuition	 |
