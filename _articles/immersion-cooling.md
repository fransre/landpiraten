---
source: TechSquare
created: '2014-05-02'
author: Jürgen
title: Immersion Cooling
categories:
- Cloud
- Architecture
---
4000 X More Efficient at Removing Heat than Air

In a two-phase (evaporative) [immersion cooled](https://www.allied-control.com/immersion-cooling) system, electronic components are submerged into a bath of dielectric heat transfer liquids, which are much better heat conductors than air, water or oil. With their various low boiling points (ie. 49°C vs. 100°C in water), they boil on the surface of the heat generating devices and rising vapor passively takes care of heat transfer. No fans or pumps are required for this process.
