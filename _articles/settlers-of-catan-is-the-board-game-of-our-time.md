---
source: TrendForum
created: '2013-04-20'
author: Jürgen
title: Settlers of Catan is the board game of our time
categories:
- Maatschappij
---
Washington Post compares two games: [Like Monopoly in the Depression, Settlers of Catan is the board game of our time](https://www.washingtonpost.com/wp-dyn/content/article/2010/11/24/AR2010112404140.html) and relates them to the challenges of their times.
