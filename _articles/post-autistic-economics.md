---
source: TrendForum
created: '2011-11-17'
author: Jürgen
title: Post-autistic economics
categories:
- Wetenschap
- Financiën
---
[Post-autistic economics](https://en.wikipedia.org/wiki/Post-autistic_economics): The movement is best seen as a forum of different groups critical of the current mainstream.

To be less offensive the movement changed the name of its magazine to [real-world economics](https://www.paecon.net/PAEReview/index.htm). Is that really less offensive?
