---
source: TechSquare
created: '2015-09-12'
author: Jürgen
title: '"Connectable" appartment'
categories:
- City
- Health
- Infrastructure
---
['Aanklikbare' woning](https://www.ed.nl/economie/aanklikbare-woning-van-wilfried-gradus-uit-eindhoven-maakt-langer-thuiswonen-mogelijk-1.5232697) van Wilfried Gradus uit Eindhoven maakt langer thuiswonen mogelijk
