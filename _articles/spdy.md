---
source: TechSquare
created: '2011-05-10'
author: Jürgen
title: SPDY
categories:
- Baseline
---
Google's browser Chrome uses a new protocol [SPDY](https://www.chromium.org/spdy/spdy-whitepaper) for communication with (all) Google services.
It is SSL based and faster than HTTP.
