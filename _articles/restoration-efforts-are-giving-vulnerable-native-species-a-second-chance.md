---
source: TrendForum
created: '2016-04-02'
author: Jürgen
title: Restoration efforts are giving vulnerable native species a second chance
categories:
- Globalisering
- Maatschappij
---
[In the Seychelles, Taking Aim at Nature’s Bullies](https://www.nationalgeographic.com/magazine/2016/03/seychelles-islands-nature-reserve-national-parks/)

- Restoration ecologists see things differently, invoking the principle “You break it, you fix it.” Humans introduced alien species, either intentionally or accidentally, and those species have altered island ecosystems, in some cases shattering them beyond recognition.
- Restoration seeks to level the ecological playing field. And sometimes the only way to do that is to remove the bullies from the schoolyard.
- Kaiser-Bunbury explained that the goal of restoration is rebuilding ecosystem integrity and functionality, not reverse-engineering a landscape that existed a hundred, or a thousand, or ten thousand years ago. It’s not about slavishly re-creating the picture on an old jigsaw puzzle box, but letting the living pieces of a fragmented system reconnect themselves and recover their historic trajectory.
