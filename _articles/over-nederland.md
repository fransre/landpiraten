---
source: TrendForum
created: '2022-10-15'
author: Jürgen
title: Over Nederland en Nederlanders
categories:
- Maatschappij
---

Wikipedia; [Rampjaar 1672](https://nl.wikipedia.org/wiki/Rampjaar_1672)

TV serie: [Het Rampjaar 1672](https://www.ntr.nl/Het-Rampjaar-1672/519)

Wikipedia: [Lotte Jensen](https://nl.wikipedia.org/wiki/Lotte_Jensen) en haar [eigen website](https://www.lottejensen.nl)

Lotte Jensen: [Wij en het water](https://www.debezigebij.nl/boek/wij-en-het-water/) <sup>[A](https://www.amazon.nl/Wij-het-water-Lotte-Jensen-ebook/dp/B09Z37PNLH)</sup>

Lotte Jensen:
- (1/5) [Sinds wanneer voelen wij ons Nederlander?](https://www.youtube.com/watch?v=-eggn-FmTqo)
- (2/5) [Hoe zorgde een Fransman ervoor dat wij meer van Nederland gingen houden?](https://www.youtube.com/watch?v=n3q8ezn6Fdo)
- (3/5) [Waarom voelen we ons meer Nederlander na een ramp?](https://www.youtube.com/watch?v=mAmpqMhGPgM)
- (4/5) [Waarom zijn wij in Nederland niet erg trots op onze vaderlandse helden?](https://www.youtube.com/watch?v=kaYq4y9A_LY)
- (5/5) [Waarom is het Wilhelmus ons volkslied geworden?](https://www.youtube.com/watch?v=1PGnRVj_YsI)

Lotte Jensen: [De mythe van het Nederlandse 'wij-gevoel'](https://www.youtube.com/watch?v=mLQ1jhp0DFc)
- Carl von Clausewitz: "Oorlogvoering is slechts een voortzetting van politiek bedrijven met andere middelen"
- Michel Foucault: "Politiek is de voortzetting van oorlog met andere middelen"
- Lotte Jense: "Vrede is de voortzetting van oorlog met andere middelen"
  - omdat vrede een proces van in en uitsluiting is
  - rampen zijn een tijd van samenhorigheid

Lezing Lotte Jensen, [Typisch Nederlands](https://www.youtube.com/watch?v=NNxya_7fpio)

TV serie: [Het verhaal van Nederland](https://hetverhaalvannederland.ntr.nl)

TV serie: [Ik ben Nederlander](https://www.npostart.nl/ik-ben-nederlander/VPWON_1266048)
- Twaalfdelige kinderserie waarin acteur Jack Wouterse met kinderen uit heel Nederland openhartige gesprekken voert over hun leven en wat hen bezighoudt.
