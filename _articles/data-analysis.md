---
source: TechSquare
created: '2013-07-21'
author: Jürgen
title: Big Data Analysis
categories:
- Privacy
- Scalability
---
some companies

- [Palantir](https://en.wikipedia.org/wiki/Palantir_Technologies) and [an example story](https://www.businessweek.com/magazine/palantir-the-vanguard-of-cyberterror-security-11222011.html)
- [Recorded Future](https://en.wikipedia.org/wiki/Recorded_Future)
- [Aqute](https://www.aqute.com/about/)

Lessig [looks back](https://billmoyers.com/segment/lawrence-lessig-on-using-coders-to-protect-our-privacy/) at his book "[Code](https://www.codev2.cc/)"([full show](https://billmoyers.com/episode/full-show-big-brother%E2%80%99s-prying-eyes/)).

Eben Moglen [at](https://www.youtube.com/watch?v=aIApDTY7eos) re:publica: "media which we watch, watches us"
[at](https://www.youtube.com/watch?v=FI1CoeqyD5o) the European Parlament

Mass surveillance systems from [Narus](https://en.wikipedia.org/wiki/Narus_%28company%29) .

Steve Lohr in his [article](https://www.nytimes.com/2010/03/17/technology/17privacy.html): "How Privacy Vanishes Online" shows publicly available partial information can be used to infer missing parts.
