---
source: TrendForum
created: '2016-02-07'
author: Willem
title: 'The Waypoint: a visual journey through Lesbos, the gateway to Europe'
categories: []
---
Someone volunteering on Lesbos sent this Washington Post [visual journey about refugees passing there](https://www.washingtonpost.com/graphics/world/lesbos/).
