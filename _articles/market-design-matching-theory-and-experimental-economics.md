---
source: TrendForum
created: '2012-10-24'
author: Jürgen
title: Market design, matching theory and experimental economics
categories:
- Wetenschap
- Maatschappij
- Financiën
---
Stanford [press conference](https://www.youtube.com/watch?v=wFod-0s_6-k) with Alwin Roth, the 2012 Nobel prize winner in economics. A lot of examples of his research are given. His first statements were that he thinks economics is part of the social sciences and even the humanities.
