---
source: TechSquare
created: '2011-02-27'
author: Jürgen
title: Dead Drops
categories:
- Privacy
---
A kind of subversive action:

Un-cloud your files in cement! ['Dead Drops’](https://deaddrops.com/) is an anonymous, offline, peer to peer file-sharing network in public space.
