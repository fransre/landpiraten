---
source: TrendForum
created: '2012-08-20'
author: Frans
title: How Americans view wealth and inequality
categories:
- Globalisering
- Maatschappij
- Financiën
---
[John Rawls](https://en.wikipedia.org/wiki/John_Rawls) said that "a just society is a society that if you knew everything about it, you'd be willing to enter it in a random place". It turns out that when people take a step away from their own position and their own current state, Americans want a much more equal society. But how can it be that people seem to want such equal society but when you look at the political ideology, people don't seem to want that?

![Champagne-Glass Distribution](https://www.imf.org/external/pubs/ft/fandd/2001/12/images/wade-c1.gif)
