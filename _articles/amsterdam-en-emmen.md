---
source: TrendForum
created: '2014-06-05'
author: Jürgen
lang: nl
title: Amsterdam en Emmen
categories:
- Maatschappij
---
Waarom iedereen in Amsterdam [wil wonen](https://www.nrcq.nl/2014/06/04/waarom-iedereen-in-amsterdam-wil-wonen-en-niemand-in-emmen) en niemand in Emmen
