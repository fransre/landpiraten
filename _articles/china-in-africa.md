---
source: TrendForum
created: '2011-06-13'
author: Jürgen
title: China in Africa
categories:
- Ontwikkelingssamenwerking
- Globalisering
---
Hillary Clinton states [criticism](https://www.washingtonpost.com/world/africa/clinton-in-zambia-to-promote-us-african-trade-and-development-womens-health-and-empowerment/2011/06/10/AG3ZOfOH_story.html) of China's involvement in Africa.

A Giant [Awakens](https://www.spiegel.de/international/world/spiegel-series-on-winners-and-losers-of-economic-boom-in-africa-a-934816.html): Inside Africa's Economic Boom

Common currency (in [Dutch](https://fd.nl/economie-politiek/802995-1312/oost-afrikaanse-landen-willen-gemeenschappelijke-munt)) in East Africa.
