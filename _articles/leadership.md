---
source: Enkairoi
created: '2009-01-03'
author: Jürgen
title: Leadership
categories:
---

### John C. Maxwell

[John C. Maxwell](https://en.wikipedia.org/wiki/John_C._Maxwell): [Today Matters: 12 Daily Practices to Guarantee Tomorrows Success](https://www.amazon.com/Today-Matters-Practices-Guarantee-Tomorrows/dp/0446529583)\
The 12 areas are:

- Choose and display the right attitude
- Determine and act on important priorities
- Know and follow healthy guidelines
- Communicate with and care for your family
- Practice and develop good thinking
- Make and keep proper commitments
- Make and properly manage your money
- Deepen and live out your spiritual faith
- Initiate and invest in solid personal relationships
- Plan for and model being generosity
- Embrace and practice good values
- Seek out and embrace personal improvements.

### Harvard Center for Public Leadership

[Youtube channel](https://www.youtube.com/user/HarvardCPL)\
mixed bag of videos, I liked Warren Bennis\
the whole idea is a bit strange at the University because they look very much at history and biography, in some way sounds like ERH\
they want to educate (grow as a gardener) leaders for the changing world, is it a science? if you are at Harvard you probably are.
