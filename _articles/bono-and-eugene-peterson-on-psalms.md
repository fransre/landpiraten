---
source: TrendForum
created: '2016-07-11'
author: Jürgen
title: Bono and Eugene Peterson on Psalms
categories:
- Kerk
- Maatschappij
---
Bono and Eugene Peterson [on Psalms](https://www.youtube.com/watch?v=-l40S5e90KY)

This short film documents the friendship between Bono (of the band U2) and Eugene Peterson (author of contemporary-language Bible translation The Message) revolving around their common interest in the Psalms.
