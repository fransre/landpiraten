---
source: TrendForum
created: '2012-05-08'
author: Jürgen
title: The Elders
categories:
- Globalisering
- Maatschappij
---
Triggered by the [announcement](https://www.trouw.nl/tr/nl/9650/Prins-Johan-Friso-bedolven-onder-lawine/article/detail/3252527/2012/05/08/Prinses-Mabel-legt-werk-bij-denktank-The-Elders-neer.dhtml) of the resignation of Mabel van Oranje as CEO of The Elders,
I looked up their [website](https://www.theelders.org/): Interesting people who really have a good reputation.

Another important Think Tank, [The International Crisis Group](https://www.crisisgroup.org/en/about.aspx) with reports about different crisis area.
