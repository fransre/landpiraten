---
source: TrendForum
created: '2011-08-19'
author: Jürgen
title: David Aikman
categories:
- Globalisering
- People
---
Dr. David Aikman is an award-winning print and broadcast journalist, a best-selling author, and a foreign affairs commentator based in the Washington, D.C. area. His wide-ranging professional achievements include a 23-year career at Time magazine, serving for several years as bureau chief in Eastern Europe, Beijing, and Jerusalem. His reporting has spanned the globe and he has covered all the major historical events of the time.

Dr. Aikman was educated at Oxford University and holds a PhD from the University of Washington in Russian and Chinese history. He is the author of many books, including Jesus in Beijing: How Christianity Is Transforming China and Changing the World Balance of Power; Billy Graham: His Life and Influence. Aikman is a frequent lecturer at Harvard University, University of the Nations, and various other colleges. He is professor of history and writer in residence at Patrick Henry College.

### David Aikman: Jesus in Beijing (2003)

***How Christianity Is Transforming China and Changing the Global Balance of Power*** [Amazon](https://www.amazon.com/Jesus-Beijing-Christianity-Transforming-Changing/dp/1596980257)

Is China America’s Next Great Ally?

An unreported tectonic shift is happening in global politics—and it’s driven by religion.

Within the next thirty years, one-third of China’s population could be Christian, making China one of the largest Christian nations in the world. And these Christians could also be China’s leaders, guiding the largest economy in the world.

What is happening in China, is what happened to the Roman Empire nearly two millennia ago. The results could be astonishing.

Veteran reporter David Aikman, former Beijing bureau chief for Time magazine takes you inside this revolution to reveal some shocking facts.

In Jesus in Beijing, you’ll learn:

- Why China might be America’s next ally against radical Islam
- Why the Chinese believe that Christianity is crucial to the rise of the West—and of China
- Why fierce anti-Christian persecution and covert government encouragement exist side-by-side in China
- Why Chinese Christians see themselves as allies of the United States—and of Israel
- How the Christian underground has spread—and won over key members of the Chinese Communist Party
- The impact of a Christianizing China on global Christianity at large

In Jesus in Beijing, David Aikman recounts the fascinating story of how Christianity began in China (even predating Francis Xavier and the Jesuits), the bloody anti-Christian persecutions (especially under the Communists), the revival of an underground Christian movement led by brave men and women risking death, and the flowering of Christianity—though still under persecution—today with the result that China is actually producing missionaries to the world.

While China’s Communist rulers hope to reap the social and economic benefits of Christianity without losing power, as David Aikman so provocatively points out, the Chinese dragon just might be tamed by the Christian Lamb. Few books change the way a reader views the world. Jesus in Beijing is one of those books.
