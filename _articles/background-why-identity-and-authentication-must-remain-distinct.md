---
source: TechSquare
created: '2011-01-19'
author: Jürgen
title: 'Why Identity and Authentication Must Remain Distinct '
categories:
- Baseline
- Security
---
nice article: [It’s Me, and Here’s My Proof: Why Identity and Authentication Must Remain Distinct](https://technet.microsoft.com/en-us/library/cc512578%28printer%29.aspx)
