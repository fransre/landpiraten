---
source: TrendForum
created: '2016-07-17'
author: Jürgen
title: World Bank Names Paul Romer as Chief Economist
categories:
- Ontwikkelingssamenwerking
- Globalisering
- Maatschappij
---
Mr. [Romer](https://en.wikipedia.org/wiki/Paul_Romer) takes over the [economic helm](https://www.wsj.com/articles/world-bank-names-paul-romer-as-chief-economist-1468712306) at the development institution when industrializing nations and the world’s poorest countries are struggling with decelerating growth, weak commodity prices, rising debt, expanding budget deficits and anemic global demand.

“A traditional explanation for the persistent poverty of many less-developed countries is that they lack objects such as natural resources or capital goods,” Mr. Romer wrote in a Concise Encyclopedia of Economics post on the Library of Economics and Liberty website. “Increasingly, emphasis is shifting to the notion that it is ideas, not objects, that poor countries lack.”

That knowledge can be appropriated by poorer nations from advanced economies to boost growth.

“If a poor nation invests in education and does not destroy the incentives for its citizens to acquire ideas from the rest of the world, it can rapidly take advantage of the publicly available part of the worldwide stock of knowledge,” Mr. Romer said.

One way the next World Bank chief economist advocates to foster such development is through “[charter cities](https://en.wikipedia.org/wiki/Charter_city).”
