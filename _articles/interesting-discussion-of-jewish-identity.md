---
source: TrendForum
created: '2010-11-06'
author: Jürgen
title: Interesting discussion of Jewish identity
categories:
- Geschiedenis
---
[The wikipedia article about Emil Fackenheim presents an interesting discussion about Jewish identity.](https://en.wikipedia.org/wiki/Emil_Fackenheim)
The core of the discussion is about the consequence of the Shoa for Jewish existence.
In conclusion it seems that this negative approach has been an important step for reviving Jewishness for many.

In the critique a good question for all groups/religions: "Why should we exist (as a group)?" If the answer remains silent, it's time to rediscover who you are, and/or to disband.
