---
source: TrendForum
created: '2012-07-22'
author: Jürgen
title: Christopher Nolan on reality
categories:
- Maatschappij
---
Christopher Nolan: "... the shared experience of watching a story unfold on screen is an important and joyful pastime. The movie theater is my home ...".
Can life really be so abstract?

Niema Hulin concludes her [article](https://www.examiner.com/article/the-psychology-of-christopher-nolan-s-films-light-of-the-colorado-shootings):
'In The Dark Knight, the film where The Joker was so prominently displayed, a line was uttered by Harvey Dent ... that is aptly appropriate for this situation. He said, “The night is darkest just before the dawn. And I promise you, the dawn is coming.” Aurora means dawn. Maybe that in itself is evidence that this community and this country will move on and continue to heal. For now, they can only hope.'
