---
source: TrendForum
created: '2016-07-17'
author: Jürgen
title: Brenninkmeijer
categories:
- Geschiedenis
- Maatschappij
- Management
- Financiën
---
Brenninkmeijer [de](https://de.wikipedia.org/wiki/Brenninkmeijer_(Familie))[en](https://en.wikipedia.org/wiki/Brenninkmeijer_family)

In 1841, the brothers Clemens and August founded the textile shop C&A, which sold, unusually for that time, ready made clothes. The two brothers, peddlers originally from the small village Mettingen in Westphalia traveled each year to Friesland to sell their textiles to the farmers. In 1861, they stocked their goods in a warehouse in Sneek. This small town in the north of the Netherlands became the location of their first store.

Die über 500-köpfige Großfamilie zählt mit einem geschätzten Vermögen von mehr als 25 Milliarden Euro zu den reichsten Familien Europas. Die Brenninkmeijers gelten als verschwiegen; öffentlichkeitswirksame Auftritte werden vermieden. Alle Familienmitglieder haben einen niederländischen Pass, jeder bekennt sich zum katholischen Glauben.

* [Topman van C&A spreekt over naziverleden familie](https://www.nrc.nl/nieuws/2016/07/14/topman-van-ca-spreekt-over-naziverleden-familie-3253927-a1511694)
* ["Verstörend und schockierend für meine Familie"](https://www.zeit.de/wirtschaft/unternehmen/2016-07/c-und-a-maurice-brenninkmeijer-ns-vergangenheit)
