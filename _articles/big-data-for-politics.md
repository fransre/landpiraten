---
source: TrendForum
created: '2013-07-19'
author: Jürgen
title: Big data for politics
categories:
- Techniek
- Maatschappij
- Financiën
---
See how the Obama 2012 campaign [profited from big data](https://swampland.time.com/2012/11/07/inside-the-secret-world-of-quants-and-data-crunchers-who-helped-obama-win/print/).
