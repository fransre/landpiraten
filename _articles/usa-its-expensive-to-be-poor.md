---
source: TrendForum
created: '2015-05-10'
author: Jürgen
title: 'USA: It’s expensive to be poor.'
categories:
- Maatschappij
---
[Where the Poor Live Dearly](https://www.zeit.de/gesellschaft/2015-05/poverty-racism-black-neighborhoods-baltimore-ferguson-new-york/komplettansicht): From Ferguson to Baltimore: Injustice and violence. What is going on in America? An attempt to find an explanation by Kerstin Kohlenberg
