---
source: TrendForum
created: '2016-04-22'
author: Jürgen
title: 'Connectography: How MegaCities Change the Map of the World'
categories:
- Globalisering
- Maatschappij
---
We’re accelerating into a future shaped less by countries than by connectivity. Mankind has a new maxim – Connectivity is destiny – and the most connected powers, and people, will win.

In this [book](https://www.paragkhanna.com/connectography) [Parag Khanna](https://en.wikipedia.org/wiki/Parag_Khanna) guides us through the emerging global network civilization in which mega-cities compete over connectivity more than borders. His journeys take us from Ukraine to Iran, Mongolia to North Korea, Panama City to Dubai, and the Arctic Circle to the South China Sea—all to show how 21st century conflict is a tug-of-war over pipelines and Internet cables, advanced technologies and market access.

Yet Connectography is a hopeful vision of the future. Khanna argues that new energy discoveries and innovations have eliminated the need for resource wars, global financial assets are being deployed to build productive infrastructure that can reduce inequality, and frail regions such as Africa and the Middle East are unscrambling their fraught colonial borders through ambitious new transportation corridors and power grids. Beneath the chaos of a world that appears to be falling apart is a new foundation of connectivity pulling it together.

[Megacities, not nations, are the world’s dominant, enduring social structures](https://qz.com/666153/megacities-not-nations-are-the-worlds-most-dominant-enduring-social-structures-adapted-from-connectography/) by Parag Khanna

Roger Bilham: [Earthquakes and Tectonic Plate motions](https://cires1.colorado.edu/~bilham/):
Los Angeles, Istanbul, Tokio and Mexiko City are located on seismic active regions. An epicentral hit on a mega- city has the potential to cause 1 million fatalities.
