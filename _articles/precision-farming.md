---
source: TrendForum
created: '2014-06-28'
author: Jürgen
title: Precision farming
categories:
- Globalisering
- Techniek
- Maatschappij
---
With [precision farming](https://en.wikipedia.org/wiki/Precision_agriculture), advanced agriculture using GPS, satellite observations and tractors with on-board computers, the farming process is performed as accurately and efficiently as possible. This is achieved by combining data from earth observations with other geospatial sources such as cadastral data, data on the quality of the soil, vegetation and protected areas. This enables the farmer to find the optimal trade-off in maximizing his yield with minimal use of fertilizers and pesticides while respecting environmental protection.
[CWI research](https://www.cwi.nl/news/2014/cwi-in-european-research-project-precision-farming)

Integrated soil–crop system management [ISSM](https://www.pnas.org/content/108/16/6399.full.pdf)

China and other rapidly developing countries face the dual challenge of increasing yields of cereal grains while at the same time reducing the negative impacts of intensive agriculture. This study introduced a model-driven integrated soil-crop system management approach that doubled farmers' average maize yields with no increase in fertilizer use. These positive results represent a success for improving both global food security and environmental sustainability.
