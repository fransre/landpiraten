---
source: TrendForum
created: '2011-11-22'
author: Jürgen
title: 2,500 Years After the Buddha, Tibetan Buddhists Acknowledge Women
categories:
- Geschiedenis
- Kerk
---
Buddhist women are celebrating a [landmark victory](https://www.huffingtonpost.com/michaela-haas/buddhism-women_b_862798.html).

[How Buddhist Women Can Achieve Equality](https://www.huffingtonpost.com/michaela-haas/daughters-of-the-buddha-discuss-how-buddhist-women-can-achieve-equality_b_2421834.html)
