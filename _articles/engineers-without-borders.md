---
source: TrendForum
created: '2014-09-29'
author: Jürgen
title: Engineers without Borders
categories:
- Ontwikkelingssamenwerking
- Globalisering
- Techniek
- Maatschappij
---
The term [Engineers Without Borders](https://en.wikipedia.org/wiki/Engineers_Without_Borders) (EWB) is used by a number of non-governmental organizations in various countries to describe their activity based on engineering and oriented to international development work. All of these groups work to serve the needs of disadvantaged communities and people all over the world through engineering solutions.
[International organization](https://www.ewb-international.org/who-we-are/)
