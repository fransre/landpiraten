---
source: TrendForum
created: '2022-02-05'
author: Jürgen
title: Against Chomsky
categories:
- Maatschappij
- Wetenschap
- Geschiedenis
---

- [David Bade e.wiki](https://en.wikipedia.org/wiki/David_W._Bade)

African Studies Global Virtual Forum: Decoloniality and Southern Epistemologies\
David Bade: [Living Theory and Theory that Kills: Language, Communication and Control](https://www.youtube.com/watch?v=pZf_yeDVLHs)
- "Colourless green ideas sleep furiously." (Chomsky) vs. "I do"
- Rosenstock-Huessy was not simply making a comment on speech act theory, an example of a performative verb; he was saying that language brings you and another person into a relationship that, in the case of “I do” alters not just one’s own life but that of another, it establishes not just one relationship but multiple relationships backwards through the past as well as forward into the future with kinship ties, and that by establishing such relationships it reveals its political basis. “I do” has the power to bring together people of different (and perhaps antagonistic) genders, families, classes, castes, nations, races and religions.
- the idea that a universal grammar underlies language is a theoretical idea that prohibits any relationship between language and 1) communication, 2) social relationships, and 3) political realities.
