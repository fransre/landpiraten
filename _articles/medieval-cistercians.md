---
source: TrendForum
created: '2017-02-18'
author: Jürgen
title: Medieval Cistercians
categories:
- Geschiedenis
- Techniek
- Maatschappij
- Financiën
---
[Constance Berman: The Medieval Cistercians](https://www.youtube.com/watch?v=yQKTruDFXBw)

[The Cistercian Evolution - The Invention of a Religious Order in Twelfth-Century Europe](https://www.upenn.edu/pennpress/book/13390.html) by Constance Hoffman Berman
