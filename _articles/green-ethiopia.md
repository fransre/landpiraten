---
source: TrendForum
created: '2018-02-12'
author: Jürgen
title: Green Ethiopia
categories:
- Ontwikkelingssamenwerking
---
[Green Ethiopia](https://www.greenethiopia.org/cms/en/content/foundation/persons/) is about support for self-development, starting with afforestation and ending with people being empowered to sustainably improve their living situation themselves.
