---
source: TrendForum
created: '2011-12-17'
author: Jürgen
title: 'Self-Help Groups: A Powerful Alternative to Traditional Microfinance'
categories:
- Ontwikkelingssamenwerking
- Financiën
---
Self-Help Groups are [described](https://www.huffingtonpost.com/michaela-haas/selfhelp-groups-a-powerfu_b_833248.html) as an alternative to traditional microfinance which is oriented towards individuals.
