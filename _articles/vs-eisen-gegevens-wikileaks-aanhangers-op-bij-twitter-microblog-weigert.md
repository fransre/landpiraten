---
source: TechSquare
created: '2011-01-08'
author: Jürgen
lang: nl
title: VS eisen gegevens WikiLeaks-aanhangers op bij Twitter – microblog weigert
categories:
- Cloud
- Privacy
---
[VS eisen gegevens WikiLeaks-aanhangers op bij Twitter – microblog weigert](https://www.nrc.nl/nieuws/2011/01/08/vs-eisen-berichten-van-wikileaks-aanhangers-op-bij-twitter/)

[Twitter Informs Users Of DOJ WikiLeaks Court Order, Didn’t Have To](https://techcrunch.com/2011/01/07/twitter-informs-users-of-doj-wikileaks-court-order-didnt-have-to/)

Ik verwacht een discussie over de cloud. Het is namelijk veel moeilijker voor een gerecht om bij individuen data op te eisen.

Look also at the RSS discussion between Dave Winer and Techcrunch,
[What I mean by the open web](https://scripting.com/stories/2011/01/04/whatIMeanByTheOpenWeb.html).

See also [Why I’m Having Second Thoughts About The Wisdom Of The Cloud](https://techcrunch.com/2011/01/10/why-im-having-second-thoughts-about-the-wisdom-of-the-cloud)
