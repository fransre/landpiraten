---
source: TrendForum
created: '2009-01-21'
author: Jürgen
title: Double Listening
categories:
- Kerk
---
Double Listening is a term used by John Stott to describe the task of Christians which want to contribute in a relevant way to our modern world.

In his book The Contemporary Christian: An Urgent Plea for Double Listening he describes his approach:
Before we attempt to teach or evangelise we must listen to God’s Word and God’s world. Only then will we be able to communicate the authentic gospel effectively. John Stott himself models this double listening as he tackles five key questions by substituting questions for answers.

1. Amid scepticism inside the church as well as outside, what is the authentic gospel?
2. In a world torn by pain and need, what characterises the obedient disciple?
3. Now that the Bible is often set aside as culturally irrelevant, how can we relate it with integrity to contemporary society?
4. Given the church’s general lack of credibility, what is her calling and how can she fulfil it?
5. In a pluralistic society and a hungry world, what is the church’s mission?
