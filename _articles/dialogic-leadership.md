---
source: TrendForum
created: '2016-03-20'
author: Jürgen
title: Dialogic Leadership
categories:
- Management
---
Dialogic leaders [cultivate these four dimensions](https://dialogos.com/files/7313/4825/5466/dialogic_leadership.pdf)—listening, suspending, respecting, and voicing—within themselves and in the conversations they have with others. Doing so shifts the quality of interaction in noticeable ways and, in turn, transforms the results that people produce.
