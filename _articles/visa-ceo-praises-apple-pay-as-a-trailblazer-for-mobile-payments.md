---
source: TechSquare
created: '2015-02-19'
author: Jürgen
title: Visa CEO praises Apple Pay as a trailblazer for mobile payments
categories:
- Baseline
- Privacy
- Security
- Human Technical-System Interfacing
---
[Speaking](https://www.zdnet.com/article/visa-ceo-apple-pay-goldman-sachs-summit) at the Goldman Sachs Technology and Internet Conference at San Francisco's Palace Hotel, Visa CEO Charlie Scharf acknowledged a number of "false starts" in the mobile payments market over the last several years.

But Apple, he argued, has completely turned that flailing trajectory around.

"Apple is the first of the really strong use cases out in the marketplace," insisted Scharf. He posited Apple Pay is exciting because "it's a platform people can see, feel, touch, use, and understand the experience."

"It also provides the roadmap for others to be really thoughtful about," Scharf observed about Apple Pay.
