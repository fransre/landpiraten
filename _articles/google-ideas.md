---
source: TrendForum
created: '2013-05-10'
author: Jürgen
title: Google Ideas
categories:
- Techniek
- Maatschappij
---
"Google Ideas is a think/do tank that explores how technology can enable people to confront threats in the face of conflict, instability or repression. We connect users, experts and engineers to research and seed new technology-driven initiatives."

Interesting list of [projects](https://www.google.com/ideas/projects/).
