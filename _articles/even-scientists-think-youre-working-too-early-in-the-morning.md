---
source: TrendForum
created: '2015-01-07'
author: Jürgen
title: Even Scientists Think You're Working Too Early in the Morning
categories:
- Wetenschap
- Maatschappij
---
Scientist confirm what we always [knew](https://www.inc.com/jill-krasny/scientists-want-us-to-start-workdays-later.html).
