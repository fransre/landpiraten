---
source: TrendForum
created: '2011-01-18'
author: Jürgen
title: Schools kill creativity
categories:
- Globalisering
---
Plea by Ken Robinson for a different school system:
all school systems have a hierarchy of

1. mathematics and languages
2. natural sciences
3. art

It is built as a linear path for becoming a university professor and comes out of the 19th century, the industrial age. It is built as a large scale organization and is a kind of university for children.
But our societal needs are rapidly changing. ADHD may be sign that kids have different needs.

Short presentation from TED:

[Do schools kill creativity?](https://www.ted.com/talks/ken_robinson_says_schools_kill_creativity.html)

Longer presentation, essentially the same plea but more examples, and some practical examples in the end:

[Sir Ken Robinson: The Element](https://fora.tv/2010/07/08/Sir_Ken_Robinson_The_Element)

iPad schools / Steve Jobs schools:
[Onderwijs voor een nieuwe tijd](https://o4nt.nl/)
['We moeten de strijd met Maurice de Hond en de Steve Jobsschool niet aangaan'](https://www.volkskrant.nl/vk/nl/3184/opinie/article/detail/3469513/2013/07/03/We-moeten-de-strijd-met-Maurice-de-Hond-en-de-Steve-Jobsschool-niet-aangaan.dhtml)
[Radical Reform: Dutch iPad Schools Seek to Transform Education](https://www.spiegel.de/international/europe/new-ipad-schools-in-holland-hope-to-revolutionize-education-a-907936.html)
['We willen een Hannah Arendtschool in plaats van een Steve Jobsschool'](https://www.volkskrant.nl/vk/nl/3184/opinie/article/detail/3469513/2013/07/03/We-moeten-de-strijd-met-Maurice-de-Hond-en-de-Steve-Jobsschool-niet-aangaan.dhtml)
