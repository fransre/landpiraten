---
source: TrendForum
created: '2013-04-02'
author: Jürgen
title: Cohousing
categories:
- Maatschappij
---
A [cohousing](https://en.wikipedia.org/wiki/Cohousing) community is a type of intentional community composed of private homes supplemented by shared facilities.
