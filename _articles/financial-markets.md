---
source: TrendForum
created: '2011-11-12'
author: Jürgen
title: Financial Markets
categories:
- Globalisering
- Financiën
---
[Quant](https://en.wikipedia.org/wiki/Quantitative_analyst): [person](https://en.wikipedia.org/wiki/List_of_quantitative_analysts) who performs quantitative analysis

VPRO Tegenlicht: [Money and Speed](https://beta.uitzendinggemist.nl/afleveringen/1059643-money-and-speed-inside-the-black-box): about how computer systems do the electronic trading.

VPRO Tegelicht: [Aftermath of a crisis](https://beta.uitzendinggemist.nl/afleveringen/1111516): Kort na aanvang van de financiële crisis van 2008 verzamelt socioloog Manuel Castells een kleine groep van internationale topintellectuelen om zich heen om dieper over de crisis na te denken.
(Globalization led to a crisis of the Institutions. New, more human institutions have to evolve.)
