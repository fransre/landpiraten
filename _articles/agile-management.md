---
source: TrendForum
created: '2011-10-18'
author: Jürgen
title: Agile Management
categories:
- Management
---
Jurgen Appelo talks in an [interview](https://www.bits-chips.nl/nieuws/bekijk/artikel/de-manager-als-goedaardig-monster-met-zes-ogen.html) about his new book: [Management 3.0.](https://www.amazon.com/Management-3-0-Developers-Developing-Addison-Wesley/dp/0321712471).

Quote from the interview: "Bureaucratische bedrijven met veel managementlagen hebben hun langste tijd gehad. Nieuwe producten komen steeds sneller op de markt en om daarin mee te kunnen, is een democratische organisatie met zelfsturende teams onontbeerlijk."
