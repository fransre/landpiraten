---
source: TrendForum
created: '2015-03-15'
author: Jürgen
title: 'Special Providence: American Foreign Policy and How It Changed the World'
categories:
- Geschiedenis
- Globalisering
---
Walter Russell Mead's 2002 book [Special Providence: American Foreign Policy and How It Changed the World](https://www.theguardian.com/books/2002/mar/16/books.guardianreview1) identifies four traditions of American diplomacy:

- Hamiltonians: They sought a system in which other nations joined international trading systems voluntarily. They were the precursors of today's proponents of globalisation. America should be its leading leading superpower, e.g Jeb Bush and Hillary Clinton
- Wilsonians: emerged from the missionary movement of the 19th century, when tens of thousands of Americans lived abroad and spread the word of God by offering education, food and medicine and the pro-motion of civil society. At home, their churches and supporters petitioned the government in much the same way as Amnesty International and certain religious groups do today, demanding that US diplomats put human rights on the agenda, e.g. US ambassador to the UN Samantha Power
- Jacksonians: are the warriors of American society. While they prefer to avoid conflict with the rest of the world and often rail at the complications of economic engagement, they believe that, if war comes, the US should deploy all its power in ruthless pursuit of total victory, e.g Ted Cruz and John McCain
- Jeffersonians: They saw the US as a "city on the hill" but they did not believe that America should promote freedom and prosperity by exporting its way of doing things. Instead, it was to teach by example. But the Jeffersonians were not classical isolationists. They were minimalists with a realist streak, e.g. Obama
