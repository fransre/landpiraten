---
source: TrendForum
created: '2018-03-18'
author: Jürgen
title: "'Platform Capitalism'"
categories:
- Globalisering
- Techniek
- Maatschappij
---
['Platform Capitalism'](https://www.youtube.com/watch?v=BMoKAn1grgQ) - Dr Nick Srnicek ([book](https://www.amazon.com/Platform-Capitalism-Theory-Redux-Srnicek/dp/1509504877))

4 types of platforms:

* advertising platforms: Google, Facebook ⇒ have to maximise the collection of data
* cloud/industrial platforms: AWS, Microsoft, Google ⇒ renting out HW/SW/algorithms
  * GE/Siemens: App Store for factories)
* product platforms: Amazon, iTunes ⇒ product to service
  * Rolls Royce rents jet engines ⇒ generates data from use
* lean platforms: Uber, AirBnB ⇒ monopoly of taxi data
  * outsourcing of costs: insurance, maintenance,
  * in the future will own everything
