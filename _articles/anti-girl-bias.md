---
source: TrendForum
created: '2011-12-17'
author: Jürgen
title: Anti-Girl Bias
categories:
- Globalisering
- Maatschappij
---
[Another story](https://www.huffingtonpost.com/michaela-haas/oh-boy-the-antigirl-bias-_b_884399.html) about the situation of girls:

- "Economists thought that gendercide would decrease as the wealth in Asian nations increases, but the recent Indian census and other studies reveal the opposite: more and more parents opt for a boy. The truth is: it is not getting better for girls, it is getting worse."
- "Glenn Fawcett suggests in Indian society the risk and burden of raising a girl is greater, no matter what caste or economic class."

Sheryl WuDunn on gender inequity:
"The centeral moral challenge of the 19th century was slavery. In the 20th century it was totalitarianism. In this century it is the oppression of women and girls throughout the world."
"In the last half century more girls have been discriminated to death than all the people killed on all the battlefield of the 20th century."
She together with her husband Nicholas Kristof wrote the book: [Half the Sky: Turning Oppression into Opportunity for Women Worldwide](https://en.wikipedia.org/wiki/Half_the_Sky:_Turning_Oppression_into_Opportunity_for_Women_Worldwide) and presented it at [TED](https://www.youtube.com/watch?feature=player_embedded&amp;v=hFgPtuzgw4o).

NYT: [Clashes Break Out in India at a Protest Over a Rape Case](https://www.nytimes.com/2012/12/23/world/asia/in-india-demonstrators-and-police-clash-at-protest-over-rape.html)

The NYT article concludes:

 Tens of thousands of rapes are reported each year in India, while many more go unreported because rape victims are often shunned and unable to marry. Even so, reports of rape are on the rise, up about 25 percent in the past six years. Surveys have suggested that India is one of the most unsafe countries in the world for women.

The roots of the problem run deep in a conservative society that is having trouble adjusting to educational and economic advances by women, long confined to the home. Demographics also play a role, with half of India’s population under 25 and female infanticide and the neglect of girls creating a growing gender imbalance.

But India’s criminal justice system, riddled with incompetence, corruption and political meddling, seems unable to respond effectively.

South Africa: 'Over 25% of [schoolgirls](https://www.bbc.co.uk/news/world-africa-21783076) HIV positive'
