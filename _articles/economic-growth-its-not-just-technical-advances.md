---
source: TrendForum
created: '2012-09-21'
author: Willem
title: 'Economic growth: it''s not just technical advances'
categories:
- Techniek
- Maatschappij
- Financiën
---
In [Link](https://networkedblogs.com/CiiUu) Adam Lent from RSA argues that "the focus on the technology at the heart of such a transformation can be misleading", and that what matters is "the way the generation of value is transformed". This seems a little step in the direction of seeing the human response to the appearance of technology as growth potential, instead of technology itself bringing 'advance'.
