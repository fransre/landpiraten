---
source: TechSquare
created: '2013-12-07'
author: Jürgen
title: Email names and dots
categories:
- Architecture
---
Stop Telling People There's a [Dot](https://www.slate.com/blogs/future_tense/2013/08/01/dots_in_gmail_addresses_what_happens_if_you_leave_out_the_period.html) in Your Gmail Address—It Doesn’t Matter
