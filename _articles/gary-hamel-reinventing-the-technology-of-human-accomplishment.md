---
source: TrendForum
created: '2013-01-31'
author: Jürgen
title: 'Gary Hamel: Reinventing the Technology of Human Accomplishment'
categories:
- Geschiedenis
- Management
---
Gary Hamel looks at the history of management and [shows](https://www.youtube.com/watch?v=aodjgkv65MM) how it has changed in the last 120 years. He suggests that management for the future is management for the people (e.g. employees first, customers second).
[Here](https://www.innovatieforganiseren.nl/innovatie-en-filmpjes/de-rol-van-management-vroeger-nu-en-de-toekomst/) an intro from a Dutch management site.
Gary started [Management Innovation eXchange](https://www.managementexchange.com/)
