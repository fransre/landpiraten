---
source: TrendForum
created: '2011-01-04'
author: Jürgen
title: Can Science Determine Human Values?
categories:
- Globalisering
- Wetenschap
---
Sam Harris, an anti-theist, argues that moral behavior (ethics) can be based on science and that moral relativism is wrong:

[Sam Harris: Can Science Determine Human Values](https://fora.tv/2010/11/10/Sam_Harris_Can_Science_Determine_Human_Values)

A good presentation, but it does not do justice to history and the transformations which happened.

[Sam Harris](https://en.wikipedia.org/wiki/Sam_Harris_%28author%29) is CEO of [Project Reason](https://en.wikipedia.org/wiki/Project_Reason) and author of [The Moral Landscape: How Science Can Determine Human Values](https://en.wikipedia.org/wiki/The_Moral_Landscape:_How_Science_Can_Determine_Human_Values) (look at the testimonies of other atheists below in the article).

In essence I think that he takes neuroscience serious and this way he can overcome scientific relativism and can give a foundation for moral behavior.
"All we have is human conversation where we're trying to influence one another to share a common project of peaceful cooperation."

bredere blog over:
[Wat heeft wetenschap met moraliteit te maken?](https://www.oplossingsgerichtmanagement.nl/ongerubriceerd/wat-heeft-wetenschap-met-moraliteit-te-maken/)
