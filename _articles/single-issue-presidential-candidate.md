---
source: TrendForum
created: '2015-10-07'
author: Jürgen
title: Single issue presidential candidate
categories:
- Maatschappij
- Financiën
---
Lawrence Lessig runs as a [candidate](https://en.wikipedia.org/wiki/Lawrence_Lessig_presidential_campaign,_2016) in the 2016 US presidential elections.

His campaign platform consists of a single proposal: The Citizen Equality Act

Lawrence describes the one [problem](https://www.ted.com/talks/lawrence_lessig_we_the_people_and_the_republic_we_must_reclaim) which he wants to address: campaign finance.

[On suspending my campaign](https://lessig.tumblr.com/post/132425036707/on-suspending-my-campaign)
