---
source: TrendForum
created: '2018-03-18'
author: Jürgen
title: Eurotopia, Europe of regions
categories:
- Globalisering
- Maatschappij
---
Eurotopia [NL](https://nl.wikipedia.org/wiki/Eurotopia) [D](https://de.wikipedia.org/wiki/Eurotopia)
[Europäische Republik](https://european-republic.eu/en/)
[VPRO: Eurotopia](https://www.vprobroadcast.com/play~VPWON_1257587~eurotopia~.html) De Europese Unie is moreel en cultureel bankroet, vindt de Duitse filosoof en historica Ulrike Guerot.

[Essay](https://www.groene.nl/artikel/schaalvergroting-maar-met-mate) Eurotopia: de terugkeer van ‘het plan Heineken’
Schaalvergroting, maar met mate
