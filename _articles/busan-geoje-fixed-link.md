---
source: TechSquare
created: '2015-05-14'
author: Jürgen
title: Busan–Geoje Fixed Link
categories:
- City
- Scalability
- Infrastructure
---
The [Busan–Geoje Fixed Link](https://en.wikipedia.org/wiki/Busan%E2%80%93Geoje_Fixed_Link) is an 8.2 kilometers bridge-tunnel fixed link that connects the South Korean city of Busan to Geoje Island. It consists of two bridges (1.87 km/1.65 km) and one immersed tunnel (48 m deep / 3.2 km long).
The [immersion tunnel project](https://www.youtube.com/watch?v=AVlfvF0u2QA) was led by the Dutch company [Strukton Immersion Projects](https://www.strukton.com/projects/busan-geoje-fixed-link/).
