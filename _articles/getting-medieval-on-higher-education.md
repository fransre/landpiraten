---
source: TrendForum
created: '2011-01-28'
author: Frans
title: Getting Medieval on Higher Education
categories:
- Geschiedenis
- Maatschappij
---
"The modern system of higher education has existed for little more than a century, and its present, decadent form has materialized only in the last few generations. While I would not seriously advocate the literal adoption of monastic rules, perhaps it is time to contemplate how we might regain our sense of purpose by revisiting the traditions of our oldest institutions."

[Getting Medieval on Higher Education](https://chronicle.com/article/Getting-Medieval-on-Higher/126008/)

Great proposal.

First, acknowledging the importance of monasteries for Western development.

Second, there seems to be more thinking about modesty in the US today. David Brooks talks about [Politics and Culture in the Age of Obama](https://fora.tv/2011/01/11/David_Brooks_Politics_and_Culture#fullprogram)

Third, should we take consequences?

Another essay by David Brooks:
About the complexity of social education versus task education: [Amy Chua is a Wimp](https://www.nytimes.com/2011/01/18/opinion/18brooks.html)
