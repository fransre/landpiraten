---
source: TrendForum
created: '2010-10-23'
author: Jürgen
title: Francis Schaeffer and L'Abri
categories:
- Geschiedenis
- Kerk
---
The Francis and Edith Schaeffer Story:

* [Part 1](https://video.google.com/videoplay?docid=678533956582349107)
* [Part 2](https://video.google.com/videoplay?docid=-3674426233063549172)
* [Part 3](https://video.google.com/videoplay?docid=5069265113888754686)

My first start to think about culture and Christianity was to read: How should we then live.

[How Should We Then Live?](https://en.wikipedia.org/wiki/How_Should_We_Then_Live%3F)

[How Should We Then Live?: The Rise and Decline of Western Thought and Culture](https://www.amazon.com/dp/1581345364)

[The movie](https://www.youtube.com/watch?v=C5fHEmGWU2o)
