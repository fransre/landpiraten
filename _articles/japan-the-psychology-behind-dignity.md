---
source: TrendForum
created: '2011-05-06'
author: Jürgen
title: 'Japan: The Psychology Behind Dignity'
categories:
- Globalisering
- Maatschappij
---
Good article about [Japanese culture](https://www.huffingtonpost.com/tijana-milosevic/japan-psychology-behind-dignity_b_840901.html) with links to supporting material.

More about Japanese culture:

[Post-Crisis Japan: Ready to Embrace the World?](https://www.huffingtonpost.com/tom-doctoroff/postcrisis-japan-ready-to_b_853125.html)
