---
source: TrendForum
created: '2020-02-22'
author: Jürgen
title: Tulip mania
categories:
- Geschiedenis
- Financiën
---
[Tulip mania I: How a single tulip was worth more than a villa](https://www.youtube.com/watch?v=FTRIJ1qcG70)

[Tulip mania II: The price can only go up!](https://www.youtube.com/watch?v=kyd1X0QbRV0)

[Tulip mania III: How Financial Bubbles Burst](https://www.youtube.com/watch?v=-YjevEvlmTU)
