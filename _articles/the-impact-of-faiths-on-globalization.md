---
source: TrendForum
created: '2010-10-20'
author: Jürgen
title: The Impact Of Faiths On Globalization
categories:
- Geschiedenis
- Globalisering
---
Professor Miroslav Volf, Lamin Sanneh, and Jose Casanova discuss how faiths will engage economy, politics and international affairs under the process of globalization.

[The Impact Of Faiths On Globalization](https://www.youtube.com/watch?v=ALWM_9cVY4A)

Casanova:
3 globalization processes:

* globalization of the immanent frame (Charles Taylor),
* globalization of world religions, denominationalism: each denomination claims to be the only one but they work together
* global process of sacralization (Durkheim) (human rights, humanity)
global denominationalism
* US created an internal denominationalism: each religion (denomination) claims to be the true authentic religion but a process of mutual recognition of one another takes place
* first only among protestant religions, then catholicism and judaism
* today islam, hinduism and buddhism
how are these processes interrelated? what do they produce at different contexts
* all our theories of religion were based of the study of the experience of protestant Christianity

avoid a dichotomy between religious and secular
avoid the notion of a simple course of secularization unto which religions react
→ multiple modernities exist even within the west

Lamin Sanneh (historian of religion):
→ making connections

* observation of Adam Smith (1776, wealth of nations): uniting the most distant parts of the world
* sea routes took over the global trade from Islam which controlled them for 700 years

Same message, but other presentation:

[Faiths as Shapers of Globalization, Prof. José Casanova](https://www.youtube.com/watch?v=0P-z--NaHgw)

Some other interesting things:

* Religion transforms in response to globalisation and meeting other religions.
* An important issue in democratisation is a majority imposing their views on minorities, like religious groups in a secular society or secularists (or other religions) in a religious society.
* "You can separate ecclesiastical institutions from a state, but you cannot separate religion and politics when you have religious citizens. To separate religion and politics means to disenfranchise religious people."
* He mentions that democracy is not a single thing; democracy was not meant for large countries like India, and yet it's very vibrant. But it's no liberal democracy in our sense, even in the sense that they accept plurality of legal structures for different groups. They reinvented democracy for Indian contexts.

Manuela Kalsky [interviews](https://www.nieuwwij.nl/video/kalsky-interviewt-casanova/) José Casanova about:

* Age of globalisation
* Searching for a new we
* Multiple religious belonging

Casanova shows the complexity of new associations with the distinctness of US, Europe and Asia.

Jose Casanova is [interviewed](https://www.youtube.com/watch?v=L1De7IsNgnw) in Ukraine. Through Ukraine goes the border between the western church and the eastern church, which Huntington identified as breakline for culture war.
