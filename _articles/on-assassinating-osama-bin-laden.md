---
source: TrendForum
created: '2011-05-07'
author: Frans
title: On assassinating Osama bin Laden
categories:
- Geschiedenis
---
Peter Leithart wrote in a [blog post](https://www.leithart.com/2011/05/03/michael-moore-is-right/):

"I am glad Osama bin Laden is dead. He was an evil man.

And I think the surgical method used to kill him is commendable. The Bible, especially Judges, endorses assassinations: Kill the head, and the body becomes powerless. Wars slaughter thousands, or hundreds of thousands of relatively innocent young men, always on both sides. War is costly, especially in human terms. Better to destroy war-mongers who start wars."

Assassinating an evil man maybe endorsed by Jewish thinking, but it isn't Christian ("love thy enemy"). It ignores the Greek contribution of empathy with the enemy, embodied by [Achilles](https://en.wikipedia.org/wiki/Achilles), who, after killing [Hector](https://en.wikipedia.org/wiki/Hector), gives the corpse to [Priam](https://en.wikipedia.org/wiki/Priam), Hector's father, so it can be buried.



A similar reasoning:

[The Psychology of Revenge: Why We Should Stop Celebrating Osama Bin Laden's Death](https://www.huffingtonpost.com/pamela-gerloff/the-psychology-of-revenge_b_856184.html)

Empathy with the suffering of the enemy is central. Seems to be less than loving thy enemy.
