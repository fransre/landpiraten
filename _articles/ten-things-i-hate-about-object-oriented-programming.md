---
source: TechSquare
created: '2011-04-24'
author: Jürgen
title: Ten Things I Hate About Object-Oriented Programming
categories:
- Baseline
---
An [essay](https://blog.jot.fm/2010/08/26/ten-things-i-hate-about-object-oriented-programming/) by Oscar Nierstrasz
