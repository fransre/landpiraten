---
source: TechSquare
created: '2012-04-30'
author: Jürgen
title: Data is the basis for an Internet business model
categories:
- Cloud
- Architecture
---
Comparison between the business models of Google and Microsoft:
[How Google's Drive helps kill Microsoft's Office](https://news.cnet.com/8301-1023_3-57422601-93/how-googles-drive-helps-kill-microsofts-office/)
