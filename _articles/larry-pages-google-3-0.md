---
source: TechSquare
created: '2011-02-07'
author: Jürgen
title: Larry Page's Google 3.0
---
[People-oriented inside view](https://www.businessweek.com/print/magazine/content/11_06/b4214050441614.htm) of Google
