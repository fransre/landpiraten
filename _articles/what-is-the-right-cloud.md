---
source: TechSquare
created: '2011-06-13'
author: Jürgen
title: What is the right Cloud?
categories:
- Cloud
- Architecture
---
An article [comparing](https://www.zdnet.com/blog/btl/philosophical-differences-the-google-cloud-vs-the-apple-cloud/50245) Google's and Apple's approach to consumer cloud services.

* Google takes a long-term perspective where everything is in the cloud and the consumer uses different "windows" for dealing with it.
* Apple uses a [file synchronization](https://en.wikipedia.org/wiki/File_synchronization) approach similar to [Dropbox](https://en.wikipedia.org/wiki/Dropbox_%28service%29). The reference is in the cloud and all devices hold copies which can be changed. Whenever a connection to the cloud is available, a device synchronizes its copy with the cloud.

I prefer the Apple/Dropbox approach.

IMAP is a mail protocol which facilitates the same approach for Email. Are there similar open protocols for other applications?

Similar [discussion](https://www.zdnet.com/videos/events/microsofts-ballmer-mocks-android-phone/6317709) about iCloud.

Eddy Cue, responsible for Internet and Services at Apple, [describes iCloud](https://www.youtube.com/watch?v=bdnWfjSuGoM)

and: Steve Jobs describes iCloud -- [in 1997](https://www.youtube.com/watch?v=yE3Ta_NK4-I).
