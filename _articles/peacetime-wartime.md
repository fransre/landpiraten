---
source: TrendForum
created: '2011-07-04'
author: Jürgen
title: Peacetime - wartime
categories:
- Management
---
[Ben Horowitz](https://en.wikipedia.org/wiki/Ben_Horowitz) wrote an interesting article about different types of management style: [Peacetime versus Wartime](https://bhorowitz.com/2011/04/15/peacetime-ceowartime-ceo/)
