---
source: TrendForum
created: '2014-03-09'
author: Jürgen
title: De Correspondent
categories:
- Maatschappij
---
[De Correspondent](https://decorrespondent.nl/home) is een [kwaliteitskrant](https://vimeo.com/75394481) op het web.

A comparable German initiative: [Krautreporter](https://krautreporter.de/das-magazin)
