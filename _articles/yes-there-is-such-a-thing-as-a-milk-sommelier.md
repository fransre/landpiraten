---
source: TrendForum
created: '2019-04-06'
author: Jürgen
title: Yes, There Is Such a Thing as a Milk Sommelier
categories:
- Maatschappij
---
[Bas de Groot appreciates milk like others would a fine wine.](https://robbreport.com/food-drink/dining/worlds-first-milk-sommelier-bas-de-groot-2726308)

[melksommelier Bas de Groot is bezeten van melk](https://deliciousmagazine.nl/2015/07/14/melksommelier-bas-de-groot-is-bezeten-van-melk)
Heus, Bas de Groot is bezeten van melk. ‘Elke koe geeft haar eigen melk. Dus krijgt elke boer in Nederland unieke melk. Welke onderdelen invloed hebben op de melksmaak fascineert mij enorm.’ Bas deed een biologische-landbouwstudie en een opleiding tot activiteitenbegeleider. Met zijn concept – de melksommelier – trekt hij het land in om iedereen te laten proeven dat de ene melk de andere niet is.

[»Die Milch, die wir kaufen, ist tot«](https://sz-magazin.sueddeutsche.de/essen-und-trinken/milch-geschmack-kuehe-sommelier-87052)
