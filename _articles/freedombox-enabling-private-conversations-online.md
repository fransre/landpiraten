---
source: TechSquare
created: '2011-08-02'
author: Jürgen
title: 'FreedomBox: Enabling private conversations online'
categories:
- Cloud
- Privacy
- Architecture
- Devices
---
The [FreedomBox initiative](https://en.wikipedia.org/wiki/FreedomBox) is a community project to develop, design and promote personal servers running free software for distributed social networking, email and audio/video communications.

It got a new push through establishing a [foundation](https://freedomboxfoundation.org/index.en.html).

The New York Times reported [about it](https://www.nytimes.com/2011/02/16/nyregion/16about.html).

Eben Moglen puts his story into political context: ["Innovation under Austerity"](https://www.youtube.com/watch?v=G2VHf5vpBy8)

[McKinsey's](https://www.mckinsey.com/insights/high_tech_telecoms_internet/disruptive_technologies) look into technological future.
Privaticing Big Data seems essential. It should be a feature of every 2.0.

[Differences](https://arkos.io/2013/04/differences-between-arkos-and-freedombox/) between arkOS and FreedomBox

[about](https://www.techworld.com.au/article/528273/arkos_building_anti-cloud_raspberry_pi_/) arkOS:

EBEN MOGLEN: [Snowden and the Future](https://snowdenandthefuture.info/index.html)
Four lectures on the consequences of the revelations of Edward Snowden.

Eben Moglen [interviews](https://www.youtube.com/watch?v=N8Sc6pUR1mA) Bruce Scheier:
Snowden, the NSA, and Free Software - Bruce Schneier + Eben Moglen

Schneier talks about his experiences be the first technical person to read the Snowden documents. Then he describes consequences and solutions.
