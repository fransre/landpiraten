---
source: TechSquare
created: '2011-01-31'
author: Jürgen
title: From web apps to mobile apps
categories:
- Cloud
- Architecture
---
Google seems to move to [mobile apps](https://online.wsj.com/article/SB10001424052748703554204576112723686094898.html). Is that a trend for repositioning the cloud?
