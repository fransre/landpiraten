---
source: TrendForum
created: '2011-09-03'
author: Jürgen
title: Model for integrating philanthropy into companies
categories:
- Ontwikkelingssamenwerking
---
Salesforce promotes its [1/1/1 model](https://www.salesforcefoundation.org/sharethemodel) to other companies.
