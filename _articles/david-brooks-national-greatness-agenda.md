---
source: TrendForum
created: '2011-01-29'
author: Jürgen
title: 'David Brooks: National Greatness Agenda'
categories:
- Globalisering
- Maatschappij
---
Another [article](https://www.nytimes.com/2010/11/12/opinion/12brooks.html) by David Brooks about a coming movement to save the US from financial disaster:
"The coming movement may be a third party or it may support serious people in the existing two. Its goal will be unapologetic: preserving American pre-eminence. It will preserve America’s standing in the world on the grounds that this supremacy is a gift to our children and a blessing for the earth."
If this really is possible, it would be a sign that the US is still alive and that American world leadership has not yet ended.
