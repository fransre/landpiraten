---
source: TrendForum
created: '2016-05-17'
author: Jürgen
title: '"Struggle of our time is between fanatics and the rest of us"'
categories:
- Geschiedenis
- Globalisering
- Maatschappij
---
[Interview](https://www.youtube.com/watch?v=xpeje4eQriM) (in English) with Amos Oz about Israel, its history and his new book 'Judas'.
