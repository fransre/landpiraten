---
source: TrendForum
created: '2017-04-01'
author: Jürgen
title: Historic relations between Judaism and Christianity
categories:
- Geschiedenis
---
Classical antiquity and Early Middle Ages:

* [Die Geburt des Judentums aus dem Geist des Christentums](https://www.amazon.de/Die-Geburt-Judentums-Geist-Christentums/dp/3161502566): Fünf Vorlesungen zur Entstehung des rabbinischen Judentums (Tria Corda) von Peter Schäfer
* [The Jewish Jesus](https://www.amazon.com/Jewish-Jesus-Judaism-Christianity-Shaped/dp/0691160953/): How Judaism and Christianity Shaped Each Other by Peter Schäfer
* [Jesus in the Talmud](https://www.amazon.com/Jesus-Talmud-Peter-Schäfer/dp/0691143188) by Peter Schäfer
* [Peter Schäfer](https://www.princeton.edu/~pschafer/) [W](https://en.wikipedia.org/wiki/Peter_Sch%C3%A4fer)
* Peter Schäfer ["Jewish Responses to the Emergence of Christianity"](https://www.youtube.com/watch?v=gF9Jq-b4zh8)
* [Did Rabbinic Judaism emerge out of Christianity?](https://www.youtube.com/watch?v=R_6Q4or2jjU) (Prof. Israel Jacob Yuval)
* [Two Nations in Your Womb](https://www.amazon.de/Two-Nations-Your-Womb-Perceptions/dp/0520258185): Perceptions of Jews and Christians in Late Antiquity and the Middle Ages by Israel Jacob Yuval
* [Yuval](https://de.wikipedia.org/wiki/Israel_Yuval) vertritt die Ansicht, die Auseinandersetzung mit dem Christentum sei die treibende Kraft hinter dem Judentum zur Zeit von Midrasch und Talmud. So behauptet Yuval:
„Wo immer Ähnlichkeiten zwischen Judentum und Christentum zu beobachten sind, dürfte es sich um christlichen Einfluss auf das Judentum handeln und nicht umgekehrt, es sei denn, die jüdischen Wurzeln des betreffenden Phänomens liegen nachweislich früher als die christlichen.“

Crusades:

* [1096](https://en.wikipedia.org/wiki/Rhineland_massacres)
