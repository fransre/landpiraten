---
source: TrendForum
created: '2014-05-07'
author: Jürgen
title: The ocean is broken
categories:
- Globalisering
- Maatschappij
---
IT was the silence that made [this voyage](https://www.theherald.com.au/story/1848433/the-ocean-is-broken/) different from all of those before it.
Not the absence of sound, exactly.

19-jarige komt met [oplossing](https://www.nrc.nl/nieuws/2014/06/04/19-jarige-heeft-oplossing-voor-rondzwervend-plastic-in-oceanen/) voor groot milieuprobleem
[The Ocean CleanUp](https://www.theoceancleanup.com/)

Another alternative project:
[PACIFIC GARBAGE SCREENING](https://pacific-garbage-screening.com/)
