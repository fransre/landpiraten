---
source: TechSquare
created: '2012-02-13'
author: Frans
title: Global Christianity and the Rise of the Cellphone
categories:
- Scalability
---
[Global Christianity and the Rise of the Cellphone](https://mobile.slashdot.org/story/12/02/11/0056208/global-christianity-and-the-rise-of-the-cellphone)

[Alan Jacobs](https://ayjay.jottit.com/) writes in the Atlantic about [Every Tribe Every Nation](https://everytribeeverynation.org/), an organization whose mission is to produce and disseminate Bibles in readable mobile-ready texts for hundreds of languages as the old missionary impulse is being turned towards some extremely difficult technical challenges.

'There's a unity among Bible translators and publishers that stands in stark contrast to the fractured, fratricidal smartphone industry.' But once these technical challenges are met, it won't be only Bibles that people can get on their mobile devices, but whole new textual worlds."
