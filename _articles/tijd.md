---
source: Enkairoi
created: '2011-02-02'
author: Jürgen
lang: nl
title: Tijd
categories:
---
Er zijn verschillende begrippen van tijd. Het bekendste begrip is die van de natuurkunde. Naast de natuurkundige is er ook nog de menslijke begrip van tijd. Wij willen hier beiden vergelijken.

Belangrijk voor de menslijke begrip van tijd is de levenstijd. Die loopt onweerbrengbaar door.

De natuurkunde abstraheert van dit feit en zegt dat voor haar gebied experimenten (oneindig) herhaalbaar zijn. Bij de beweging van een mechanisch object kan je in de tijd terug en vooruit gaan. Op scheikundig gebied geldt dat als iets minder, maar ook daar kan je het experiment herhalen. De natuurkunde heeft dadoor een eendimensionaal begrip van tijd gecreeert. Verleden en toekomst zijn verschillende waarden op een tijdsas.

Mensen ervaren tijd op een andere manier. Verleden, heden en toekomst zijn er gelijktijdig aanwezig:
- ik leef alleen in mijn heden als ik met mijzelf en anderen in overeenstemming ben,
- mijn/ons verleden/erfenis: wat doe ik altijd, wat ligt in mijn ritme/routine: herinnering
- mijn/ons toekomst: ik doe iets wat nog niet bij mijn routine hoord: anticipatie

Het gelijktijdig aanwezig zijn van heden, verleden en toekomst zijn dus onafhankelijke dingen, krachten die in mijn leven werken.

***
Peter Leithart in ["The Cross of Eugen Rosenstock-Huessy"](https://www.patheos.com/blogs/leithart/2017/09/cross-eugen-rosenstock-huessy-4/):
Time is not, as mathematics depicts it, a straight line, since a line does not distinguish qualitatively between past and future and cannot capture the multiform shape of human time. Time is an undifferentiated line for animals, which know “no future but only perfect and imperfect tenses, only processes that have ended or processes still going on at any given moment” (CF, p. 166). For humans, a timeline turns time into space. This confuses things that should be kept distinct, since our experience of time differs radically from our experience of space. Space is experienced “as a whole.” We see a whole mountain range, a whole starry sky, a million man march on the Mall all at an instant. Time, by contrast, comes to us as moments, fragments, as “a phantom moment or as innumerable phantom mo­ments.” Time becomes organized and packaged into hours, days, years, epochs only “because we say so.” Times exist because “they are history-made units built by our faith, out of innumerable moments” (CF, p. 167). Time experienced this way is never uniformly one time: “Nobody lives in one time” (CF, p. 167). The past and future always inhabit the present, and so human beings are always being pulled back­ward by the obligations imposed by the past and striving forward by the hopes seducing us from the future.

***
Rosenstock-Huessy identificeert deze drie tijden met liefde, hoop en geloof.\
**Liefde** voor het aanpakken van de dingen die nu belangrijk zijn, **hoop** voor die dingen uit het verleden die nog een toekomst hebben en **geloof** voor het zetten van compleet nieuwe stappen.\
Geloof, hoop en liefde moeten basis termen gaan worden van een vernieuwde sociale wetenschap.

***
Noten:

Otto Kroesen: Een rode draad door de tijd

.... Er zijn momenten waarop het er op aan komt: elk woord krijgt een oneindig gewicht, bijvoorbeeld als partners bij elkaar de test nemen of hun liefde echt is. Dan is ‘Het Uur U’ aangebroken. Op andere tijden kan het meer ontspannen toegaan. Op weer andere tijden is iedereen verheugd en in een hoge stemming bij een feestelijke gelegenheid of bij een verrassende ontmoeting.

Op verschillende tijden neemt onze ervaring van Gods tegenwoordigheid een andere gestalte aan. Deze ervaringen kunnen eindeloos variëren, maar we kunnen er wat systeem in brengen door die ervaringen in drieën in te delen en ze aan te duiden met de drie termen Schepping, Verzoening, Verlossing. In de schepping komen we in aanraking met de erfenis van het verleden. Verzoening plaatst ons in het heden van ontmoeting en openbaring. De verlossing is gericht op de toekomst. Theologische uitgedrukt klinkt dat als volgt: In de schepping ontmoeten we God als Vader, in de Verzoening ontmoeten we God als Zoon en in de verlossing ontmoeten we God als Heilige Geest. Uit deze drie stappen zijn al onze ervaringen en wederwaardigheden ‘opgebouwd’.\
.....\
Deze drie ‘grondervaringen’ moduleren ons bestaan in de tijd. Hoe dat gebeurt dat hopen we in het nuvolgende een beetje inzichtelijk te maken.

Drie grondervaringen? We moeten er nog een vierde aan toe voegen. Want we bestaan niet alleen in de tijd. Er is naast het heden van de ontmoeting, het heden van ‘Het Uur U’ nog een ander heden, en dat is het heden van de ruimte, het naast- elkaar waar alles op zijn plek komt te staan en blijft waar het is. De ruimte is als het ware een vierde dimensie van de tijd. De ruimte is de tijd van de indicativus, de tijd waarin de dingen aangewezen worden en stil staan. Ze zijn zoals ze zijn: materie, objecten, spullen, instituten. Alles wat er altijd maar zo is, dat is er in de ruimte, niet na, maar naast elkaar. Je kunt erop terugkomen, en dan is het er nog. Niet altijd, want wij zijn steeds bezig de ruimte opnieuw in te richten door middel van techniek, door bestuursmaatregelen, door b.v. een verhuizing en noem maar op. Welnu, deze herinrichting van de ruimte vindt plaats vanuit onze gang door te tijd, d.w.z. als laatste stap van een proces. Hebben wij een nieuwe liefde gevonden, dan gaan we misschien ook ergens anders wonen. Steeds verandert eerst de tijd. Aan het einde ook de ruimte. (pagina 11)

***
duitse wikipedia heeft de beste bijdrage over Augustinus:
Zeitauffassung

„Was also ist die Zeit? Wenn niemand mich danach fragt, weiß ich's, will ich's aber einem Fragenden erklären, weiß ich's nicht.“

– Confessiones lib. 11; ebenso die folgenden Zitate

Augustinus spricht über drei Zeiten: Gegenwart des Vergangenen, Gegenwart des Gegenwärtigen und Gegenwart des Zukünftigen. Vergangenheit, Gegenwart und Zukunft als solche existieren nach Augustinus nicht:

„Wie kann man sagen, dass [die vergangenen und zukünftigen Zeiten] sind, da doch die vergangene schon nicht mehr und die zukünftige noch nicht ist? Die gegenwärtige aber, wenn sie immer gegenwärtig wäre und nicht in Vergangenheit überginge, wäre nicht mehr Zeit, sondern Ewigkeit.“

Vielmehr ist die Vergangenheit eine Erinnerung in der Gegenwart, und die Zukunft eine Erwartung in der Gegenwart, während die Gegenwart selbst, ein aus der Zukunft in die Vergangenheit an unserem Geiste vorüberziehender Moment ist. Wir messen die Zeit anhand eines

„Eindruck[s], den die vorübergehenden Dinge [in unserem Geiste] hervorbringen und der bleibt, wenn sie vorübergegangen sind, ihn, den gegenwärtigen, [messen wir], nicht was vorübergegangen ist und ihn hervorgebracht hat.“

Das augustinische Zeitverständnis enthält damit eine subjektive Komponente der Zeit, da wir die vergangene Zeit als Eindruck nur in unserem Geiste messen können, wir also in uns verschiedene erlebte Zeiträume miteinander vergleichen und dadurch immer zu subjektiven Aussagen gelangen müssen, so kam uns zum Beispiel jene Zeit länger vor, als eine andere. Zukünftige Dinge können wir nicht messen, da wir noch nichts über sie aussagen können, erst wenn sie an uns vorüberziehen und wir dadurch einen Eindruck gewonnen haben, können wir für uns entscheiden, ob jener Eindruck länger oder kürzer war.

Dennoch ist Augustinus kein reiner Zeitsubjektivist, da für ihn die Zeit immer noch untrennbar mit den Dingen und der Welt verbunden sind:

„Ginge nichts vorüber, gäbe es keine vergangene Zeit; käme nichts auf uns zu gäbe es keine zukünftige Zeit; wäre überhaupt nichts, gäbe es keine gegenwärtige Zeit.“

Auch ist für Augustinus Zeit real und keine reine Ichzeit, da Gott sie geschaffen hat. Augustinus Zeitbegriff ist also subjektimmanent aber nicht rein subjektiv.

Trotzdem steht dieses Verständnis im krassen Gegensatz zu der platonischen objektiven Zeitauffassung, in der die Zeit die Bewegung von Himmelskörpern ist, so ist zum Beispiel die Vollendung eines Tages die Bewegung von Sonnenaufgang bis Sonnenuntergang. Dagegen führt Augustinus an, dass

„wenn sich ein Körper bewegt, [wir mit der Zeit messen], wie lange er sich bewegt, und zwar vom Anfang bis zum Ende seiner Bewegung, […] denn ein Körper bewegt sich nur in der Zeit“

und stellt diese selbst nicht dar. Und auch wenn sich ein Körper nicht bewegt, sind wir doch in der Lage seinen Stillstand zu messen und etwas über die Dauer seines Stillstandes auszusagen, genau deshalb kann Bewegung nicht gleich Zeit sein.
