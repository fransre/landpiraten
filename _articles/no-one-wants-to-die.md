---
source: TrendForum
created: '2011-10-06'
author: Jürgen
title: No one wants to die
categories:
- Overig
---
"No one wants to die. Even people who want to go to heaven don't want to die to get there. And yet death is the destination we all share. No one has ever escaped it. And that is as it should be, because Death is very likely the single best invention of Life. It is Life's change agent. It clears out the old to make way for the new."

Quote from the [text](https://news.stanford.edu/news/2005/june15/jobs-061505.html) of the [speech](https://www.youtube.com/watch?v=D1R-jKKp3NA) of Steve Jobs, Stanford, 2005

A Sister’s [Eulogy](https://www.nytimes.com/2011/10/30/opinion/mona-simpsons-eulogy-for-steve-jobs.html?pagewanted=all)
