---
source: TrendForum
created: '2011-01-29'
author: Jürgen
title: 'David Brooks: The Sporting Mind'
categories:
- Geschiedenis
- Maatschappij
---
[Nice article](https://www.nytimes.com/2010/02/05/opinion/05brooks.html) were David Brooks looks at the cultural origins of sports and their associated traditions. Our sports are a mixture of these 3 traditions and very important for current moral formation.
