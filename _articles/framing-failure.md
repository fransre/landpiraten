---
source: TrendForum
created: '2016-02-22'
author: Willem
title: Framing failure
categories: []
---
"We can run into cognitive dissonance if we realize that we did something that wasn't actually effective. Why did I do this thing? why did we do this thing? There's an urge to rationalize it. The [Wikimedia FailFest & Learning Pattern hackathon 2015](https://upload.wikimedia.org/wikipedia/commons/6/64/FailFest_and_Learning_Pattern_Hackathon_-_Berlin_2015.pdf) recommends that we try framing our stories about our past mistakes to avoid that temptation." With questions to ask for "Big 'F'" and "Little 'f'" failure framing (p. 17-18).

Tool 3 of [this blogpost](https://www.harihareswara.net/sumana/2016/02/19/1) on FLOSS community metrics at [FOSDEM](https://fosdem.eu/).
