---
source: TrendForum
created: '2011-01-14'
author: Jürgen
title: 'STEPS Manifesto: Otto Kroesen'
categories:
- Ontwikkelingssamenwerking
- Globalisering
---
Innovation, Sustainability, Development: A New Manifesto
"If you had to make one recommendation to the UN, or another global body, about the future of innovation for sustainability and development, what would it be?"
- invest in an intercultural dialogue not only in technology:
[Otto Kroesen - Manifesto](https://www.youtube.com/watch?v=jDQJlPoEafk)
