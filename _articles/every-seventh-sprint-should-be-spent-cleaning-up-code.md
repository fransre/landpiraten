---
source: TechSquare
created: '2011-02-15'
author: Frans
title: Every seventh sprint should be spent cleaning up code
---
Quote from the book [Coders at Work](https://nuclear.dnsalias.com/books/coders_at_work.pdf) applying a biblical principle to computer coding:

Seibel (the interviewer): In one of your talks you quoted Exodus 23:10 and 11: “And six years thou shalt sow thy land, and shalt gather in the fruits thereof: But the seventh year thou shalt let it rest and lie still” and suggested that every seventh sprint should be spent cleaning up code. What is the right time frame for that?

Crockford (the interviewee): Six cycles—whatever the cycle is between when you ship something. If you’re on a monthly delivery cycle then I think every half year you should skip a cycle and just spend time cleaning the code up.

Seibel: So if you don’t clean up every seventh cycle you may be faced with the choice of whether or not to do a big rewrite. How do you know when, if ever, it’s time for a big rewrite?

Crockford: Generally the team knows when it’s time. Management finds out a lot later. The team is getting beat up pretty regularly, making too many bugs; the code’s too big, it’s too slow; we’re falling behind. They know why. It’s not because they became stupider or lazier. It’s because the code base is no longer serving the purpose that it needs to.
