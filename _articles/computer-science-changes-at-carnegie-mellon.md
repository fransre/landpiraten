---
source: TechSquare
created: '2011-03-28'
author: Willem
title: Computer science changes at Carnegie Mellon
categories:
- Education
---
[report](https://reports-archive.adm.cs.cmu.edu/anon/2010/CMU-CS-10-140.pdf) (from [article](https://existentialtype.wordpress.com/2011/03/15/teaching-fp-to-freshmen/) ).

Two interesting changes: object oriented programming is removed from freshmen curriculum, and the use of a scripting language (Python) instead of Java.
