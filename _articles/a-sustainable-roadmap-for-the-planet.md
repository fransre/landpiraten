---
source: TrendForum
created: '2011-06-14'
author: Frans
title: A Sustainable Roadmap for the Planet
categories:
- Geschiedenis
- Globalisering
- Techniek
---
[Bjørn Lomborg](https://en.wikipedia.org/wiki/Bj%C3%B8rn_Lomborg) (director of the [Copenhagen Consensus Center](https://www.copenhagenconsensus.com/CCC%20Home%20Page.aspx) and the author of [The Skeptical Environmentalist](https://www.amazon.com/Skeptical-Environmentalist-Measuring-State-World/dp/0521804477/) and [Cool It](https://www.amazon.com/Cool-Skeptical-Environmentalists-Global-Warming/dp/0307266923/)) explains [How to Save the Planet](https://www.newsweek.com/2011/06/12/bjorn-lomborg-explains-how-to-save-the-planet.html):

"We forget too easily that innovation and ingenuity have solved most major problems in the past. Living sustainably means learning the lessons from history. And chief among those is that the best legacy we can leave our descendants is to ensure that they are prosperous enough to respond resiliently to the unknown challenges ahead."

He's serious about sustainability while not just saying "stop", period. Thanks.
