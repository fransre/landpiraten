---
source: TechSquare
created: '2013-12-07'
author: Jürgen
title: PhotoDNA
categories:
- Baseline
---
[PhotoDNA](https://en.wikipedia.org/wiki/PhotoDNA) robust algorithm for identifying photos under scaling and colour changes
