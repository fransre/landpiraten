---
source: TechSquare
created: '2011-01-17'
author: Jürgen
title: Limits of multi-tasking
categories:
- Human Technical-System Interfacing
---
Going for the technologically possible is not a good design, what would be a good one?
[In New Military, Data Overload Can Be Deadly](https://www.nytimes.com/2011/01/17/technology/17brain.html?_r=1&hp=&pagewanted=all)
Mindfulness is the keyword, origins from Buddhism, but it is used in all kind of psychological support, even in Christian ones.
