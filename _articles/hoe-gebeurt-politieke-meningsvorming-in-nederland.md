---
source: TrendForum
created: '2011-01-29'
author: Jürgen
lang: nl
title: Hoe gebeurt politieke meningsvorming in Nederland?
categories:
- Maatschappij
---
Wat is de toekomst van de politieke meningsvorming in Nederland? En hoe gebeurt er een Christelijke inbreng?
Zijn het Christelijke partijen die binnen een verzuilingsmodel hun kiezers achter hun scharen? Is hun rol dus meer op hun zelf gericht: "de eigen identiteit beschermen is tot op heden een doel gebleven"?
Hier twee artikelen, die daar tegen in gaan.
[Schud de last van de verzuiling van je af](https://www.protestant.nl/actueel/schud-de-last-van-de-verzuiling-van-je-af)
[Heeft confessionele partijvorming nog zin?](https://www.refdag.nl/nieuws/politiek/netwerkdag_heeft_confessionele_partijvorming_nog_zin_1_528163)
Ik mis echter nog iets. Namelijk de vraag wat de positieve bijdrage zou moeten zijn.
Zouden we niet toch Otto Kroesen moeten volgen en naar de culturele 'verworvenheden' kijken die onze westerse geschiedenis gevormd hebben. Als we deze consequent toepassen, hebben we volgens mij genoeg te doen.
Christelijke partijen of niet wordt dan een pragmatische keuze.
