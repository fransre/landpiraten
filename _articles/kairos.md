---
source: Enkairoi
created: '2008-12-22'
author: Jürgen
title: Kairos
categories:
---

Kairos (καιρος) is an ancient greek word that means the right time. In contrast to the word chronos (χρονος), meaning the chronological time of a clock, kairos is the right time for something to happen: it is concerned with our actions.

We consider it relevant to "read the time" and listen to today's questions, and possibly tomorrow's; to respond at the appropriate time; to act at the right moment - ἐν καιρῳ.
- [What is kairos?](https://markfreier.files.wordpress.com/2018/05/kairos-study.pdf) at whatifenterprises
- [Kairos](https://en.wikipedia.org/wiki/Kairos) at Wikipedia
