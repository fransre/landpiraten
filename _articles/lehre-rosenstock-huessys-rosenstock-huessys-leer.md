---
source: EensVoorAl
created: '2017-07-09'
author: Jürgen
title: Lehre Rosenstock-Huessy's / Rosenstock-Huessys leer
---
Zusammengefaßt in 3 Begriffen: Sprache, Geschichte und Zeit

Für alle drei Zweige werden dann je vier Themen genannt:
1. Das Kreuz der Wirklichkeit,
2. Die Leiblichkeit alles Sprechens,
3. Der Vorrang der Namengebung,
4. Die Grammatik als Gesellschaftskunde,
5. Revolutionen und Epochen als Sprachereignisse,
6. Die vier antiken und die drei nachchristlichen Weltalter,
7. Die Frucht der Lippen als Zeitenschoß,
8. Die Argonautik als Überwindung der Akademik,
9. [Die Zwölftonreihe der Zeitigung]({{ 'rosenstock-huessy-12-tones-of-the-spirit' | relative_url }}),
10. Die Überwindung der Zeit-Ungenossenschaft,
11. Die Unterscheidung von Glaube und Hoffnung,
12. Die wirtschaftende Gesellschaft als zukünftige Partnerin der Kirche.

==
Samengevat in drie begrippen: spraak, geschiedenis, tijd:

Voor alle drie terreinen worden vier onderwerpen genoemd:
1. Het kruis van de werkelijkheid
2. De lichamelijkheid van al het spreken
3. Het primaat van de naamgeving
4. De grammatica als maatschappijleer
5. Revoluties en tijdperken als talige verworvenheden
6. De vier antieke en de drie na-christelijke wereldtijdperken
7. De vrucht der lippen als baarmoeder van de tijden
8. De argonautiek als overwinning op het academisch denken
9. [De twaalftonenreeks van de tijdsbeleving]({{ 'rosenstock-huessy-12-tones-of-the-spirit' | relative_url }})
10. De overwinning op onze ongelijktijdigheid
11. Het verschil tussen geloof en hoop
12. De economische samenleving als toekomstige partner van de kerk
