---
source: TrendForum
created: '2009-01-02'
author: Jürgen
title: Richard Florida
categories:
- Globalisering
- Techniek
- People
---
![Richard Florida](https://creativeclass.com/richard_florida/pix/hdr_rich.png)

[Richard Florida](https://en.wikipedia.org/wiki/Richard_Florida) ([his site](https://creativeclass.com/richard_florida)) wrote books about the [Creative Class](https://creativeclass.com/) (its [rise](https://www.amazon.com/Rise-Creative-Class-Transforming-Community/dp/0465024777) and [flight](https://www.amazon.com/Flight-Creative-Class-Global-Competition/dp/B001JJBOYW)).The Creative Class are the people who change the world and are located at a small number of centers around the world.
His latest book is called: [Who's Your City?: How the Creative Economy is Making Where to Live the Most Important Decision of Your Life](https://www.amazon.com/Whos-Your-City-Creative-Important/dp/0465003524).
Richard presents his book at Google (here are the [maps](https://creativeclass.com/whos_your_city/maps)).

[Video](https://www.youtube.com/watch?v=Cj1OpiBRNsg)

A very good introductory video about the Creative Class.

[Video](https://www.youtube.com/watch?v=iLstkIZ5t8g)

The claim of Richard is contrary to the claim by [Thomas Friedman](https://en.wikipedia.org/wiki/Thomas_Friedman) ([his site](https://www.thomaslfriedman.com)) in his book [The World is Flat](https://en.wikipedia.org/wiki/The_World_is_Flat) (on [Amazon](https://www.amazon.com/World-Flat-3-0-History-Twenty-first/dp/0312425074)).

[Criticism](https://info.interactivist.net/node/12887) of Richard Florida in Toronto.
