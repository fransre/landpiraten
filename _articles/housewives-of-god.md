---
source: TrendForum
created: '2010-11-15'
author: Frans
title: Housewives of God
categories:
- Geschiedenis
- Kerk
---
![Housewives of God](https://graphics8.nytimes.com/images/2010/11/14/magazine/14evangelicals-span/14evangelicals-t_CA0-articleLarge.jpg)

In centuries past, evangelical women were not meek about their role in church. Early Baptists allowed women to preach during the Great Awakening, and women were among the most influential revivalists during the rise of Pentecostalism at the turn of the 20th century (though gender roles usually remained in force in the home).

[Housewives of God](https://www.nytimes.com/2010/11/14/magazine/14evangelicals-t.html)
