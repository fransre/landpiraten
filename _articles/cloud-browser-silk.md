---
source: TechSquare
created: '2011-09-29'
author: Jürgen
title: 'Cloud browser: Silk'
categories:
- Cloud
- Architecture
- Devices
---
Amazon is introducing it's new [split-browser](https://www.infoworld.com/d/applications/amazons-silk-browser-one-ups-operas-approach-the-web-174387) for the Kindle Fire.

Now if this were seamlessly move between rendering on the device and different cloud providers. That would also prevent the "walled garden". [Link](https://www.itworld.com/security/208851/closing-net)
