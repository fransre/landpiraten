---
source: TechSquare
created: '2011-06-14'
author: Jürgen
title: On privacy and the Internet
categories:
- Privacy
---
[Interesting article](https://www.zdnet.com/blog/google/privacy-worries-google-shouldnt-be-your-biggest-fear/2995) about danger of privacy violations. He separates three parts of our lives: secret, private and public.

German [initiative](https://www.idgard.de/relaunch/video/why_idgard.webm) for web privacy.
[How](https://www.idgard.de/relaunch/video/how_idgard.webm) it works.
[Some](https://www.idgard.de/en/business/dl_documents_e/) white papers.

Some interesting persons:
- [Caspar Bowden](https://en.wikipedia.org/wiki/Caspar_Bowden)
- [Max Schrems](https://en.wikipedia.org/wiki/Max_Schrems)
- [William Binney](https://en.wikipedia.org/wiki/William_Binney_%28U.S._intelligence_official%29)

Some [Spiegel articles](https://www.spiegel.de/international/topic/nsa_spying_scandal/) about NSA spying.
