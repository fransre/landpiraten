---
source: TrendForum
created: '2011-07-18'
author: Jürgen
title: 'Civilization: The West and the Rest'
categories:
- Geschiedenis
- Kerk
- Globalisering
- Wetenschap
- Techniek
- Maatschappij
---
[Niall Ferguson](https://en.wikipedia.org/wiki/Niall_Ferguson), the current partner of [Ayaan Hirsi Ali](https://en.wikipedia.org/wiki/Hirsi_Ali) and father of her expected child, has written a [new book](https://www.amazon.com/gp/product/1846142733) about the reasons why the west rules the rest for the last 400 years. He identified 6 reasons, which he calls killer apps (a software metaphor that everybody understands, as he hopes):

* competition
* science
* private property connected with political representation
* medicine
* consumerism
* (protestant) work ethic

As an adherent of [virtual history](https://en.wikipedia.org/wiki/Virtual_history) [see his [book](https://www.amazon.com/Virtual-History-Counterfactuals-Niall-Ferguson/dp/0465023231)], he compares the West with the most likely winner of the time.
To illustrate how the killer apps were an advantage, he compares the West

* for competition with China (e.g. the spice race),
* for science with the Ottomans,
* for private property and democracy with South America (including the original sin of racial discrimination and slavery)
* for medicine with Africa (including the pseudoscience of eugenics)
* for consumerism with the East Block (Jeans conquered communism)
* for the work ethic referring to Protestant Christianity (and mentions its current manifestation in China).

Channel 4 created a [six part series](https://www.youtube.com/watch?v=arhBShGlZjI) based on the book.

Interesting analysis and sharp definition of the "killer apps". Very insightful series, with a remaining question: is "ruling the world" still possible or even desirable in our time?

More books on global history and development:

* [Why Nations Fail](https://www.amazon.de/Why-Nations-Fail-Origins-Prosperity/dp/0307719219) by [Daron Acemoglu](https://en.wikipedia.org/wiki/Daron_Acemo%C4%9Flu) and James Robinson,
[review](https://www.guardian.co.uk/books/2012/mar/11/why-nations-fail-acemoglu-robinson-review)

* [How the West Was Lost: Fifty Years of Economic Folly--and the Stark Choices Ahead](https://www.amazon.com/How-West-Was-Lost-Folly--/dp/0374533210) by [Dambisa Moyo](https://en.wikipedia.org/wiki/Dambisa_Moyo),
[review](https://www.guardian.co.uk/books/2011/jan/16/west-lost-dambisa-moyo-review)
