---
source: TrendForum
created: '2011-05-09'
author: Frans
title: On the current state of academia
categories:
- Wetenschap
---
[Reform the PhD system or close it down](https://www.nature.com/news/2011/110420/full/472261a.html):

One reason that many doctoral programmes do not adequately serve students is that they are overly specialized, with curricula fragmented and increasingly irrelevant to the world beyond academia. Expertise, of course, is essential to the advancement of knowledge and to society. But in far too many cases, specialization has led to areas of research so narrow that they are of interest only to other people working in the same fields, subfields or sub-subfields. Many researchers struggle to talk to colleagues in the same department, and communication across departments and disciplines can be impossible.

If doctoral education is to remain viable in the twenty-first century, universities must tear down the walls that separate fields, and establish programmes that nourish cross-disciplinary investigation and communication. They must design curricula that focus on solving practical problems, such as providing clean water to a growing population.

AI as an [example](https://www.technologyreview.com/computing/37525/?p1=A3&a=f):

Winston blamed the stagnation [of AI, Frans] (...) on ever-narrower specialties such as neural networks or genetic algorithms. "When you dedicate your conferences to mechanisms, there's a tendency to not work on fundamental problems, but rather [just] those problems that the mechanisms can deal with," said Winston.

Winston said he believes researchers should instead focus on those things that make humans distinct from other primates (...).

An [article](https://www.nytimes.com/2011/03/15/books/merkels-possible-successor-resigns-in-plagiarism-scandal.html) about the relevance of a PhD in Germany and one of three recent cases where prominent people lost their PhD due to plagiarism.

Further, the article compares the German obsession with academic titles with the American obsession with sex.
