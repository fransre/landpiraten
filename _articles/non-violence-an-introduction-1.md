---
source: TrendForum
created: '2011-06-13'
author: Willem
title: 'Non-violence: an introduction (1)'
categories:
- Overig
---
Violence, who doesn't know it? Our history contains many examples. We see it daily in the newspaper; almost all movies contain violence. And while we might be against it in most cases, we resort to it so easily when we are out of options. It's part of our thinking.

What does violence do? It is use of (physical) power to force another to act as you wish. What happens to the one against whom violence is used? Sometimes violence solves situations, the visible "source" is killed. But what remains is always loss, pain, anger and hate. Not always this is immediately visible, but it goes its way and all too often results in a new situation of violence. How can we bring peace into this world by war? How can we bring goodness into this world by means of something bad?

Is there an alternative? What to do in hopeless situations where two parties stand against each other irreconcilable? Is it possible that there is another solution than violence, without being naive? And what if the other does use violence?

While I've found it hard to believe that there is an answer, the many examples I've encountered are beginning to convince me that the alternative is real and works in practice.

 * [The Search for a Nonviolent Future](https://www.mettacenter.org/nv/resources/publications): book by [Michael Nagler](https://en.wikipedia.org/wiki/Michael_N._Nagler)
 * [How To Start A Revolution](https://www.howtostartarevolutionfilm.com/): documentary about [Gene Sharp](https://en.wikipedia.org/wiki/Gene_Sharp) and his ideas
 * [Non-Violent Struggle](https://www.colorado.edu/conflict/peace/treatment/nonviolc.htm) at an Online Training Program on Intractable Conflict at the [University of Colorado](https://www.colorado.edu/)

Some [historical examples](https://en.wikipedia.org/wiki/Non-violent_resistance).
