---
source: TrendForum
created: '2013-03-02'
author: Jürgen
title: Double Irish With a Dutch Sandwich
categories:
- Globalisering
- Financiën
---
Innovative taxation [design](https://en.wikipedia.org/wiki/Double_Irish_arrangement) used by some Internationals, e.g. [Google](https://www.theregister.co.uk/2012/11/22/how_vendors_avoid_tax/).

Ireland to close ‘[double Irish](https://www.theguardian.com/business/2014/oct/13/ireland-close-double-irish-tax-loophole)’ tax loophole
