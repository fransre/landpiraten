---
source: TrendForum
created: '2014-10-29'
author: Jürgen
lang: nl
title: De Nederlandse Agro & Food sector staat internationaal in hoog aanzien.
categories:
- Globalisering
- Techniek
---
Theo Bruinsma (Marel) [over de Agro & Food sector](https://www.euroforum.nl/blog/theo-bruinsma-marel-we-hebben-de-invloed-van-de-technologie-de-agro-food-zeer-sterk-zien-binnenkomen/):

- “We hebben de invloed van de technologie in de agro food, zeer sterk zien binnenkomen”
- “Wij moeten de rest van de wereld leren hoe je op een omgevingsvriendelijke wijze een hele grote productie in stand moet houden. Dat is in mijn ogen een van onze grootste exportproducten.”


Philips opent een [landbouwlab](https://www.ed.nl/economie/philips/altijd-zomer-in-landbouwlab-van-philips-op-high-tech-campus-eindhoven-1.5006736) op 6 juli.
