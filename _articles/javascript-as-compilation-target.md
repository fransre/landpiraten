---
source: TechSquare
created: '2012-09-15'
author: Jürgen
title: Javascript as compilation target
categories:
- Architecture
- Devices
---
Next to generating HTML from some more friendly human readable language as wikis, there is also JavaScript generation:
Dropbox [rewrote](https://tech.dropbox.com/?p=361) their browser-side code base in CoffeeScript.
