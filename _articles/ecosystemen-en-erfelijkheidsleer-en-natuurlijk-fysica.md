---
source: TechSquare
created: '2011-12-03'
author: Jürgen
lang: nl
title: Ecosystemen en erfelijkheidsleer; en natuurlijk fysica
categories:
- Baseline
- Architecture
---
Een interessante essay over [vooruitgang in de techniek](https://www.mechatronicamagazine.nl/index.php?id=1100&tx_ttnews[tt_news]=25214&no_cache=1) en de rol van fysica daarin.
