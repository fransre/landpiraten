---
source: TechSquare
created: '2014-02-13'
author: Jürgen
lang: nl
title: Fairphone niet 100% eerlijk
categories:
- Devices
---
Initiatiefnemer van de Fairphone, Bas van Abel, [vertelt](https://www.nettoresultaat.net/?p=11) onomwonden dat ook zijn mobiele telefoons niet 100% eerlijk zijn.

The [CFTI](https://solutions-network.org/site-cfti/) (Conflict Free Tin Initiative) supply chain is a conflict-free design, piloting new tracking and tracing procedures to ensure the conflict-free status of the supply chain.

[Solutions for Hope](https://solutions-network.org/site-solutionsforhope/) Project Offers Solutions and Brings Hope to the People of the Democratic Republic of Congo (DRC)

The ‘Solutions for Hope Project’ was launched as a pilot initiative to source conflict-free tantalum from the DRC.
