---
source: TrendForum
created: '2011-10-23'
author: Jürgen
title: Girl Effect
categories:
- Ontwikkelingssamenwerking
- Globalisering
---
[Girl Effect](https://www.girleffect.org/) is designed to educate the general public on the unique role adolescent girls can play in eradicating global poverty.

Some statistics [about](https://www.huffingtonpost.com/michaela-haas/unleashing-the-power-of-t_b_1007669.html) the girl effect.
