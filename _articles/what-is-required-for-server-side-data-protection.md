---
source: TechSquare
created: '2011-04-27'
author: Jürgen
title: What is required for server-side data protection?
categories:
- Privacy
- Architecture
---
The [Sony incident](https://arstechnica.com/gaming/news/2011/04/sony-admits-utter-psn-failure-your-personal-data-has-been-stolen.ars) poses the question how digital identities of consumers are protected. Do we not need explicit requirements and (public) auditing for data storage of companies and public institutions?

For instance, at Amazon, I can not read my credit card number even when I am logged in. For identification I get to see the last four numbers only. It is a write-once (no read, no update, only delete) and use via other functions (e.g. buying transaction) logic.

Technically, there should be different databases which are synchronized but protected differently. That is probably also necessary for scaling and performance as different use cases have different characteristics.
