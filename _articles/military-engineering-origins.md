---
source: TrendForum
created: '2015-05-15'
author: Jürgen
title: 'Military engineering: origins'
categories:
- Geschiedenis
- Techniek
---
The word engineer was initially used in the context of warfare, dating back to 1325 when engine’er (literally, one who operates an engine) referred to "a constructor of military engines". In this context, "engine" referred to a military machine, i. e., a mechanical contraption used in war (for example, a catapult).

As the design of civilian structures such as bridges and buildings matured as a technical discipline, the term [civil engineering](https://en.wikipedia.org/wiki/Civil_engineering) entered the lexicon as a way to distinguish between those specializing in the construction of such non-military projects and those involved in the older discipline. As the prevalence of civil engineering outstripped engineering in a military context and the number of disciplines expanded, the original military meaning of the word “engineering” is now largely obsolete. In its place, the term [military engineering](https://en.wikipedia.org/wiki/Military_engineering) has come to be used.
