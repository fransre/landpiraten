---
source: Enkairoi
created: '2009-02-15'
author: Jürgen
title: Jeffery Sachs
categories:
- People
---

### Jeffery Sachs videos

[Jeffery Sachs videos](https://www.earth.columbia.edu/users/profile/jeffrey-d-sachs)

[video](http://www.columbia.edu/acis/networks/advanced/ei/fall2008/1020_arch.ram) of a good discussion by [Soros](https://en.wikipedia.org/wiki/George_Soros), [Roubini](https://en.wikipedia.org/wiki/Nouriel_Roubini) and [Sachs](https://en.wikipedia.org/wiki/Jeffrey_Sachs)
also see Sachs's new book: [Common Wealth](https://www.amazon.com/Common-Wealth-Economics-Crowded-Planet/dp/0143114875):
- here an introduction by [atGoogleTalks](https://www.youtube.com/watch?v=n3kzzVP2c7w)
