---
source: TrendForum
created: '2015-02-23'
author: Jürgen
title: What ISIS Really Wants
categories:
- Globalisering
- Maatschappij
---
The Islamic State is no mere collection of psychopaths. It is [a religious group with carefully considered beliefs](https://www.theatlantic.com/features/archive/2015/02/what-isis-really-wants/384980/), among them that it is a key agent of the coming apocalypse. Here’s what that means for its strategy—and for how to stop it.
by Graeme Wood

Here a non-literalistic reaction: [What The Atlantic Gets Dangerously Wrong About ISIS And Islam](https://thinkprogress.org/world/2015/02/18/3624121/atlantic-gets-dangerously-wrong-isis-islam/)
