---
source: Enkairoi
created: '2009-02-15'
author: Jürgen
title: Kishore Mahbubani
categories:
- People
---

[Kishore Mahbubani](https://mahbubani.net/) [Wikipedia](https://en.wikipedia.org/wiki/Kishore_Mahbubani) knows the Eastern and Western world alike and explains the rise of Asia by their application of Western values.

## The New Asian Hemisphere: The Irresistible Shift of Global Power to the East

In a quite remarkable book called [The New Asian Hemisphere: The Irresistible Shift of Global Power to the East](https://mahbubani.net/new-asian-hemisphere-the-irresistible-shift-of-global-power-to-the-east/) [Amazon](https://www.amazon.com/New-Asian-Hemisphere-Irresistible-Global/dp/1586484664), he states that Asia is aspiring the values of the West but does not want to copy the behaviour of the West. He suggests that the West should embrace their new Asian companions instead of fearing to lose their monopoly of power in the world.

### Chapter 1: The Three Scenarios

In the past millennia, the Asian civilisations have had an important place in the world. During the last centuries years, however, the West has made a large leap, leaving the rest of the world behind. It took a while, but Asia is coming back, precisely because it recognises the lessons from the West and takes them to heart. This march to modernity is currently taking place and brings prosperity:
- **personal**: reduction of poverty, giving people more self-worth and freedom to act (since there is time to make choices instead of struggling to survive)
- **social**: improved safety, health and education
- **global**: a more stable world because there is less reason for war.

Mahbubani describes possible three reactions of the West to Asia's rise in three scenario's:
1. March to modernity: the West embraces the East and together they form a stable and global society
2. Retreat into fortresses: the West is scared by the success of Asia and wants to protect it's own interests. This illustrated by Sarkozy's words from June, 2007: "The word 'protection' is no logner taboo" N. While the globalisation of the market has profited the economy for the West as well as the East, the West is becoming more and more afraid of open trading and investment. Since 1945 the US annual incomes have increased an estimated US$9000 per household because of trade liberalisation; and developed countries have seen an annual income gain of US$142 billion from the elimination of trade barriers, which exceeds the US$120 billion in economic help by the major industrialised countries (2005). The West is also afraid to rebalance power in global organisations as the UN, IMF and World Bank; Europe, for example, still has 25 places in many of them while other countries as big as Europe have only one.
3. Triumph of the West: after the second world war, the West and Russia were the only superpowers. Now that Russia has changed, the West sees itself as the only superpower, and wants to help other countries obtaining the same success. This is both generous and arrogant. Mahbubani describes three fundamental flaws in the post-world-war triumphalist view:
   1. Economy is important, not values. Russia has shown that this is not working.
   2. Anyone can become in an instant a liberal democracy.
   3. Culture differences are unimportant

### Chapter 2: Why Asia is rising now

Mahbubani describes that "there were at least seven pillars of Western wisdom that could have an almost miraculous effect on their societies. Each pillar reinforces the effect of the other. By starting to implement these seven pillars, Asian societies have taken off."
- Free-market economics
- Science and technology
- Meritocracy
- Pragmatism
- Culture of peace
- Rule of law
- Education

What has happened in the West is truly remarkable. There is not only absence of war (among the Western states), but there is zero prospect of war: countries don't want it. He explains that free market economics have created a new middle class in all countries. And a middle class is not interested in war any more.
- "If you want to solve the problems of the world, it's very easy. Just implement the values of the West".
- "The West is not true to it's own values."

He is very optimistic about the future of the world.

### Further information

- He [discusses](https://www.youtube.com/watch?v=wc4eMtAqcvo) his book with Harry Kreisler.
- ["De eeuw van Azie"](https://www.vpro.nl/programmas/tegenlicht/kijk/afleveringen/2008-2009/de-eeuw-van-azie.html), Mahbubani on dutch television (Sept 2008)
- ["The next supermodel"](https://www.vpro.nl/programmas/tegenlicht/kijk/afleveringen/2008-2009/the-worlds-next-supermodel.html), dutch television program about new macro-economic models where also Mahbubani discusses his view (Feb 2009)
