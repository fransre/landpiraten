---
source: TrendForum
created: '2011-07-02'
author: Frans
title: What really drives the poor
categories:
- Ontwikkelingssamenwerking
---
Economists [Abhijit Banerjee](https://en.wikipedia.org/wiki/Abhijit_Banerjee) and [Esther Duflo](https://en.wikipedia.org/wiki/Esther_Duflo) wrote a book on [Poor Economics](https://en.wikipedia.org/wiki/Poor_Economics): A Radical Rethinking of the Way to Fight Global Poverty ([short article](https://www.livemint.com/2011/06/24201858/What-really-drives-the-poor.html)):

"Generally, it is clear that things that make life less boring are a priority for the poor." (...) "They don’t just need nutrition that will keep them alive longer - they also need a reason to want to live longer."

Interesting read. It almost seems as if they advocate to give more money to poor people. But I recognise that poor people often don't have the inner space in their lives to care about long-term things like immunization and education, and that these small immediate benefits could help them; it might be a step in reversing the spiral.
