---
source: TrendForum
created: '2016-07-02'
author: Jürgen
title: Open-book management
categories:
- Management
---
The basis of [open-book management](https://en.wikipedia.org/wiki/Open-book_management) is that the information received by employees should not only help them do their jobs effectively, but help them understand how the company is doing as a whole.

[Dutch example](https://www.nrc.nl/handelsblad/2016/06/25/hier-leest-iedereen-elkaars-mail-2880244)
