---
source: Enkairoi
created: '2009-01-03'
author: Jürgen
title: Culture of Improvement
categories:
---

### A Culture of Improvement: Technology and the Western Millennium
by Robert Friedel

Why does technology change over time, how does it change, and what difference does it make? In this sweeping, ambitious look at a thousand years of Western experience, Robert Friedel argues that technological change comes largely through the pursuit of improvement—the deep-rooted belief that things could be done in a better way. What Friedel calls the "culture of improvement" is manifested every day in the ways people carry out their tasks in life—from tilling fields and raising children to waging war.

Improvements can be ephemeral or lasting, and one person’s improvement may not always be viewed as such by others. Friedel stresses the social processes by which we define what improvements are and decide which improvements will last and which will not. These processes, he emphasizes, have created both winners and losers in history.

Friedel presents a series of narratives of Western technology that begin in the eleventh century and stretch into the twenty-first. Familiar figures from the history of invention are joined by others—the Italian preacher who described the first eyeglasses, the dairywomen displaced from their control over cheesemaking, and the little-known engineer who first suggested a grand tower to Gustav Eiffel. Friedel traces technology from the plow and the printing press to the internal combustion engine, the transistor, and the space shuttle. Friedel also reminds us that faith in improvement can sometimes have horrific consequences: improved weaponry makes warfare ever more deadly and the drive for improving human beings can lead to eugenics and even genocide. The most comprehensive attempt to tell the story of Western technology in many years, engagingly written and lavishly illustrated, A Culture of Improvement documents the ways in which the drive for improvement has shaped our modern world.

Nice review of Friedels book: [A Culture of Improvement: Technology and the Western Millennium](https://www.amazon.com/Culture-Improvement-Technology-Western-Millennium/dp/0262062623/) by [The NYT](https://www.nytimes.com/2007/05/21/arts/21conn.html)
