---
source: TrendForum
created: '2016-04-18'
author: Jürgen
title: Digital Society
categories:
- Globalisering
- Techniek
- Maatschappij
---
[Dirk Helbing](https://en.wikipedia.org/wiki/Dirk_Helbing): [The Automation of Society is Next](https://www.amazon.com/Automation-Society-Next-Survive-Revolution/dp/1518835414)

After the automation of factories and the creation of self-driving cars, the automation of society is next. But there are two kinds of automation: a centralized top-down control of the world, and a distributed control approach supporting local self-organization. Using the power of today's information systems, governments and companies like Google seem to engage in the first approach.

- [NervousNet.Info](https://nervousnet.info/)
- [Team@NervousNet](https://www.nervousnet.ethz.ch/team/)
- [about NervousNet](https://futurict.blogspot.nl/2016/01/nervousnet-towards-open-and.html)
