---
source: TechSquare
created: '2015-10-15'
author: Jürgen
title: Self-Driving Cars
categories:
- Devices
- Human Technical-System Interfacing
---
Some short-term success:
* Self-driving cars will [cruise the factory floor](https://www.zdnet.com/article/self-driving-cars-for-the-factory-floor/) before they're big on the open road
