---
source: TrendForum
created: '2016-11-04'
author: Jürgen
lang: nl
title: Wim van der Leegte
categories:
- Techniek
- Maatschappij
- Management
- People
---
Wim van der Leegte: hoe een slimme zakenman de rijkste man van Brabant werd

EINDHOVEN - Wim van der Leegte gaat dinsdag na vijftig jaar trouwe dienst met pensioen. De directeur-eigenaar van VDL Groep draagt het stokje over aan zijn zoon Willem. Van der Leegte is meer dan een directeur van een topbedrijf. Naast miljardair is hij ook trotse Brabander en familiemens. Een [portret](https://www.omroepbrabant.nl/?news/2568661283/Wim+van+der+Leegte+hoe+een+slimme+zakenman+de+rijkste+man+van+Brabant+werd.aspx) van een bijzondere maar vooral succesvolle zakenman.
