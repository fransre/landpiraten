---
source: TechSquare
created: '2013-07-23'
author: Jürgen
title: Security-based DB (re)-design
categories:
- Security
- Scalability
---
"Apple Developer Website Update
	
Last Thursday, an intruder attempted to secure personal information of our registered developers from our developer website. Sensitive personal information was encrypted and cannot be accessed, however, we have not been able to rule out the possibility that some developers’ names, mailing addresses, and/or email addresses may have been accessed. In the spirit of transparency, we want to inform you of the issue. We took the site down immediately on Thursday and have been working around the clock since then.

In order to prevent a security threat like this from happening again, we’re completely overhauling our developer systems, updating our server software, and rebuilding our entire database.
We apologize for the significant inconvenience that our downtime has caused you and we expect to have the developer website up again soon."

Here the news/discussion about [Mailpile](https://www.ghacks.net/2013/08/03/mailpile-private-secure-open-source-email-service/): a private, secure, open source, locally-run email service.

Secure email services: [Posteo](https://posteo.de/site/datenschutz), [Runbox](https://runbox.com/why-runbox/email-privacy-offshore-email/)

[Riseup](https://www.riseup.net/en/about-us) provides online communication tools for people and groups working on liberatory social change. We are a project to create democratic alternatives and practice self-determination by controlling our own secure means of communications.

[Core Infrastructure Initiative](https://www.linuxfoundation.org/news-media/announcements/2014/04/amazon-web-services-cisco-dell-facebook-fujitsu-google-ibm-intel):
Amazon Web Services, Cisco, Dell, Facebook, Fujitsu, Google, IBM, Intel, Microsoft, NetApp, Rackspace, VMware and The Linux Foundation Form New Initiative to Support Critical Open Source Projects
