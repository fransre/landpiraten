---
source: TrendForum
created: '2013-10-19'
author: Jürgen
title: Third culture kid
categories:
- Ontwikkelingssamenwerking
- Kerk
- Globalisering
---
A [Third Culture Kid](https://en.wikipedia.org/wiki/Third_culture_kid) (TCK) is a person who has spent a significant part of his or her developmental years outside the parents' culture. The TCK frequently builds relationships to all of the cultures, while not having full ownership in any. Although elements from each culture may be assimilated into the TCK's life experience, the sense of belonging is in relationship to others of similar background.
