---
source: TrendForum
created: '2011-03-25'
author: Frans
title: 23 Things They Don’t Tell You About Capitalism
categories:
- Globalisering
- Maatschappij
---
All 23 things from [Ha-Joon Chang](https://en.wikipedia.org/wiki/Ha-Joon_Chang)’s book [23 Things They Don’t Tell You About Capitalism](https://www.amazon.com/Things-They-Dont-About-Capitalism/dp/1608191664/):

 1. There is no such thing as a free market.
 2. Companies should not be run in the interest of their owners.
 3. Most people in rich countries are paid more than they should be.
 4. The washing machine has changed the world more than the Internet.
 5. Assume the worst about people and you will get the worst.
 6. Greater macroeconomic stability has not made the world economy more stable.
 7. Free-market policies rarely make poor countries rich.
 8. Capital has a nationality.
 9. We do not live in a post-industrial age.
 10. The U.S. does not have the highest standard of living in the world.
 11. Africa is not destined for underdevelopment.
 12. Governments can pick winners.
 13. Making rich people richer doesn't make the rest of us richer.
 14. U.S. managers are overpriced.
 15. People in poor countries are more entrepreneurial than people in rich countries.
 16. We are not smart enough to leave things to the market.
 17. More education in itself is not going to make a country richer.
 18. What is good for General Motors is not necessarily good for the United States.
 19. Despite the fall of communism, we are still living in planned economies.
 20. Equality of opportunity may be not be fair.
 21. Big government makes people more open to change.
 22. Financial markets need to become less, not more, efficient.
 23. Good economic policy does not require good economists.

Read more in [this review](https://www.tfdnews.com/news/2011/03/24/88287-ian-fletcher-review-hajoon-changs-23-things-they-dont-tell-you-about-capitalism.htm) or in [the notes from a lecture at NYU](https://10tonfunk.tumblr.com/post/4052159020/notes-from-ha-joon-changs-lecture-at-nyu-on-23-things).

The review gives a good impression of the points.
