---
source: TechSquare
created: '2011-03-01'
author: Jürgen
title: 'Cloud requirements: disconnected backup and incremental user base SW update'
categories:
- Cloud
- Architecture
---
The errors in Gmail and [Google's actions](https://gmailblog.blogspot.com/2011/02/gmail-back-soon-for-everyone.html) reveal important requirements for good cloud software:

- disconnected backup (here on tapes)
- incremental user base SW update to limit the effect of faults
