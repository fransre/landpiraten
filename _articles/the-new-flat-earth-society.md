---
source: TrendForum
created: '2011-03-28'
author: Willem
title: '"The new flat earth society"'
categories:
- Wetenschap
- Maatschappij
---
This [scientific satire article](https://jclahr.com/bartlett/flat-earth.html) was [published](https://dx.doi.org/10.1119/1.2344473) in [The Physics Teacher](https://aapt.org/) in 1996. Apart from the obvious it made me think that there is an understanding among scientists (sorry for stereotyping) that society influences science, but that this may be a negative thing in the first place.
