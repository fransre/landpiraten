---
source: TrendForum
created: '2014-12-01'
author: Jürgen
title: Kamikaze sisters profile Lebanon’s jihadis
categories:
- Globalisering
- Maatschappij
---
Paying keen attention to detail, the Yamout sisters spent two years cultivating professional relationships with 20 prisoners in Roumieh prison accused of terrorism-related offenses. Their research sheds light both on the lack of criminal psycho-social profiling in Lebanon and the complex society which has emerged on the third floor of Roumieh’s Block B, where suspected terrorists are held. [...](https://dailystar.com.lb/News/Lebanon-News/2014/Nov-24/278668-kamikaze-sisters-profile-lebanons-jihadis.ashx)
