---
source: Enkairoi
created: '2009-05-30'
author: Jürgen
title: Creative Class
categories:
---

### Richard Florida

[Richard](https://creativeclass.com/richard_florida/) [Wikipedia](https://en.wikipedia.org/wiki/Richard_Florida) wrote books about the Creative Class (its rise and flight).The Creative Class are the people who change the world and are located at a small number of centers around the world.
His latest book is called: ["Who's Your City?: How the Creative Economy is Making Where to Live the Most Important Decision of Your Life"](https://www.amazon.com/Whos-Your-City-Creative-Important/dp/0465003524)\
Richard presents his [book](https://www.youtube.com/watch?v=Cj1OpiBRNsg) at Google.
[Here](https://creativeclass.com/whos-your-city/maps/) the maps.\
A very good introductory video about the [Creative Class](https://www.youtube.com/watch?v=iLstkIZ5t8g)\
The claim of Richard is contrary to the claim by [Thomas Friedman](https://www.thomaslfriedman.com/) [Wikipedia](https://en.wikipedia.org/wiki/Richard_Florida) in his book [The World is Flat](https://en.wikipedia.org/wiki/The_World_is_Flat) [Amazon](https://www.amazon.com/World-Flat-3-0-History-Twenty-first/dp/0312425074).

[Criticism](https://www.thestar.com/news/insight/2009/06/27/why_richard_floridas_honeymoon_is_over.html) of Richard Florida in Toronto.
