---
source: TechSquare
created: '2015-04-27'
author: Jürgen
title: The smart sock
categories:
- Health
- Devices
---
Kenneth Shinozuka: My simple [invention](https://www.youtube.com/watch?v=tFX1nQLZUVM), designed to keep my grandfather safe

Sixty percent of people with dementia wander off, an issue that can prove hugely stressful for both patients and caregivers.
To solve this problem Kenneth Shinozuka has designed smart devices for his grandfather, who has Alzheimer's disease: a pair of smart socks, which send an alert to a caregiver if a patient gets out of bed.
