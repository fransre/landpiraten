---
source: TechSquare
created: '2013-09-06'
author: Jürgen
title: 'Schneier: re-engineer the internet'
categories:
- Privacy
- Policy
---
Bruce Schneier wrote an essay: [The US government has betrayed the internet. We need to take it back. The NSA has undermined a fundamental social contract. We engineers built the internet – and now we have to fix it](https://www.theguardian.com/commentisfree/2013/sep/05/government-betrayed-internet-nsa-spying)
"... the US has proved to be an unethical steward of the internet. The UK is no better. "

Yochai Benkler states: [Time to tame the NSA behemoth trampling our rights](https://www.theguardian.com/commentisfree/2013/sep/13/nsa-behemoth-trampling-rights)

Bruce Schneier: "[The NSA, Snowden, and Surveillance](https://www.youtube.com/watch?v=3apzxHAA8mI)"

By [manipulating Internet traffic](https://www.zdnet.com/americans-as-vulnerable-to-nsa-surveillance-as-foreigners-despite-fourth-amendment-7000031045/) to push American data outside of the country, the NSA can vacuum up vast amounts of US citizen data for intelligence purposes, a new report warns.
