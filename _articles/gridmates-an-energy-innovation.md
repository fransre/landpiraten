---
source: TrendForum
created: '2015-07-25'
author: Jürgen
title: Gridmates - An energy innovation
categories:
- Globalisering
- Techniek
- Maatschappij
- Financiën
---
[Gridmates](https://www.smartgridnews.com/special-reports/gridmates-potentially-world-changing-energy-innovation) - A potentially world-changing energy innovation

How Gridmates is [crowdsourcing electricity](https://www.techrepublic.com/article/how-gridmates-is-crowdsourcing-electricity-to-help-eliminate-energy-poverty) to help eliminate energy poverty
