---
source: TrendForum
created: '2015-09-24'
author: Jürgen
title: Robin-Hood-Index
categories:
- Maatschappij
- Financiën
---
Would the world be a better place if the wealthiest gave their fortunes away to the bottom billion? We tried to answer the question by creating the [Robin Hood Index](https://www.nrcq.nl/2015/09/24/wat-als-de-rijkste-persoon-in-elk-land-zijn-geld-gaf-aan-de-armen).

Quantifying the boost from each payout is hard because each dollar will buy you something different in local currency terms. The poverty line also is different from nation to nation. Nevertheless, a modern-day Robin Hood it seems would do little to improve the lives of those in need.
