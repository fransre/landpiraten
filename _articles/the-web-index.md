---
source: TechSquare
created: '2014-01-30'
author: Jürgen
title: The Web Index
categories:
- Privacy
- Security
- Policy
---
Designed and produced by the World Wide Web Foundation, the [Web Index](https://thewebindex.org/about/who-we-are/) is the first multi-dimensional measure of the World Wide Web’s contribution to development and human rights globally. It covers 81 countries, incorporating indicators that assess the areas of *universal access*; *freedom and openness*; *relevant content*; and *empowerment*.

Established by Web inventor Sir Tim Berners-Lee, the World Wide Web Foundation is a non-profit organization which seeks to establish the open Web as a global public good and a basic right, ensuring that everyone can access and use it freely.


[Web Index: Berners Lee warns against abuse of privacy](https://www.youtube.com/watch?v=TxkJp2qrjuQ) proposes a proper system of checks and balances, every system where the government needs to break privacy, a public institution which surveys this operation needs to be installed.
[Sir Tim Berners-Lee's manifesto for the internet](https://www.youtube.com/watch?v=vK8IvFspLmA)

Alan Rusbridger, editor-in-chief of the [Guardian](https://www.theguardian.com/) ([part1](https://www.youtube.com/watch?v=GmodvbWpvGc),[part2](https://www.youtube.com/watch?v=7bWQsTXB_pw)) about Glenn Greenwald and Edward Snowden

EFF praises major tech companies for [doing more](https://www.engadget.com/2014/05/16/eff-transparency-report-shows-more-companies-standing-up-for-us/) to protect your data.
