---
source: TrendForum
created: '2014-09-30'
author: Jürgen
title: Competition Is for Losers
categories:
- Globalisering
- Management
- Financiën
---
Competition Is for Losers
If you want to create and capture lasting value, look to build a monopoly,
[writes](online.wsj.com/articles/peter-thiel-competition-is-for-losers-1410535536) Peter Thiel


[Zero to one](https://en.wikipedia.org/wiki/Zero_to_One) [A](https://www.amazon.com/Zero-One-Notes-Startups-Future/dp/0804139296)
