---
source: TechSquare
created: '2011-01-11'
author: Jürgen
title: 'Never buying a laptop again: Motorola Atrix'
categories:
- Architecture
- Devices
---
[Short video](https://www.youtube.com/watch?v=kDgnybcmbiA)
or
[longer, noisy but with TV and RC instead of keybord and MM-PC](https://www.youtube.com/watch?v=ZX15TrMjR3w)
Looks like the future!

[Technical description](https://www.zdnet.com/blog/btl/breakthrough-device-of-ces-2011-motorola-atrix-phone-pc/43406)

[The next Wintel: Android plus NVIDIA](https://www.techrepublic.com/blog/hiner/the-next-wintel-android-plus-nvidia/7454): about the big players in mobile space.

[Meet Google's secret weapon for fighting Apple and Microsoft](https://news.cnet.com/8301-1035_3-57417863-94/meet-googles-secret-weapon-for-fighting-apple-and-microsoft/)

.... Chau revealed that Webtop and Android run on the same Linux kernel. "They're not side-by-side," said Chau. "They're running together." He also said that there's no virtualization involved. The bottom line: It would be relatively straightforward for Google to integrate Webtop into the native Android code without complicated software engineering.
