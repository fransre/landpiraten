---
source: TrendForum
created: '2011-10-08'
author: Jürgen
title: Missional living
categories:
- Kerk
- Maatschappij
---
["Missional living"](https://en.wikipedia.org/wiki/Missional_living) is a Christian term that describes a missionary lifestyle; adopting the posture, thinking, behaviors, and practices of a missionary in order to engage others with the gospel message.
