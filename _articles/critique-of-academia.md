---
source: TrendForum
created: '2023-04-29'
author: Jürgen
title: Critique of academia
categories:
- Wetenschap
---
[Grievance studies affair](https://en.wikipedia.org/wiki/Grievance_studies_affair)

[Sokal affair](https://en.wikipedia.org/wiki/Sokal_affair)

[List of scholarly publishing stings](https://en.wikipedia.org/wiki/List_of_scholarly_publishing_stings)

[Publish or perish](https://en.wikipedia.org/wiki/Publish_or_perish)


An earlier article: [On the Current State of Academia]({{ '/on-the-current-state-of-academia' | relative_url }})
