---
source: TrendForum
created: '2011-05-11'
author: Jürgen
title: Encountering Eugen Rosenstock-Huessy
categories:
- Geschiedenis
- Kerk
- Globalisering
- Maatschappij
---
Darrol Bryant describes his own [journey](https://thetravelingguru.org/Traveling_Guru_Darrol_Bryant_More_1_files/Encountering%20Eugen%20Rosenstock-Huessy.pdf) with Rosenstock-Huessy. During his life, he discovered Rosenstock-Huessy's relevance for social work, history and interfaith dialogue.

ERH Biography in [Progress](https://erhbiography.blogspot.nl/) by Raymond Huessy
