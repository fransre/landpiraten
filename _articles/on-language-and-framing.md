---
source: TrendForum
created: '2009-08-08'
author: Jürgen
title: On language and framing
categories:
- Wetenschap
---
Notes on George Lakoff

[Idea Framing, Metaphors, and Your Brain](https://www.youtube.com/watch?v=S_CWBjyIERY) - George Lakoff
-----------
1. Every institution is structured by a frame:
2 elements:
roles or frame elements
scenarios
2. Every word in every language is defined relative to a frame
3. Metaphor: e.g. more is up

Georg Lakoff: [The Political Mind](https://www.youtube.com/watch?v=saDHFomGW3A)
----------------

The old idea of reason, derived from Descartes, is based on
enlightenment principles.

Properties of the enlightenment mind:

reason is supposed to be
1. conscious: you are supposed to know what you are thinking
2. dispassionate: emotions are supposed to get in the way of reason
3. literal: applying directly to the world
4. logical: if you give somebody the facts they should be able to reason to the right conclusion
5. disembodied
6. based on self interest: the reason you have reason is to pursue your self interest
7. universal: everybody is supposed to reason in the same way

After 30 years of neuro and cognitive science we know that everyone of those properties is false.

People (in politics) care about
-------------------------------

1. values, not just issues and problems
2. communicate, connect with people
3. do you say the truth, mean what you say, be authentic
4. can people trust you
5. can people identify with you

This is not irrational, the old conception of rationality is wrong.

Here is an [introductory article](https://andrelevy.net/lakoff_framing101.pdf) of Lakoff about framing.
