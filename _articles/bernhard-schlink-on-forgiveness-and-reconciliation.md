---
source: TrendForum
created: '2010-10-13'
author: Jürgen
title: Bernhard Schlink on forgiveness and reconciliation
categories:
- Geschiedenis
---
[Bernhard Schlink: Guilt About the Past](https://www.amazon.com/Guilt-About-Past-Bernhard-Schlink/dp/0887849598)

[Interessante smaakmaker van 5 minuten](https://www.youtube.com/watch?v=o1WQML0hfl4)

[Een presentatie van een uur](https://www.abc.net.au/tv/bigideas/stories/2009/08/28/2669983.htm).

Ik vond het begin van de smaakmaker zelfs nog wat abstract, maar zijn voorbeelden aan het eind maakten me echt benieuwd naar wat hij nog meer te zeggen heeft. Eigenlijk gaat het om eerlijkheid, denk ik. Bernhard zegt regelmatig: die verontschuldiging klinkt niet goed. En zijn opmerkingen over stammen en solidariteit snijden hout.
Ik herkende veel hiervan uit de film [La siciliana ribelle](https://www.imdb.com/title/tt1213926/). Het gaat over de dochter van een maffiabaas die probeert met zichzelf en haar verleden in het reine te komen en met name de liefdevolle relatie met haar vader maakt het haar moeilijk afstand van hem te nemen en hem te zien als een misdadiger.
