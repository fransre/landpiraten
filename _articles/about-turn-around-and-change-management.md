---
source: TrendForum
created: '2015-06-02'
author: Jürgen
title: About turn-around and change management
categories:
- Globalisering
- Management
---
Christoph Mueller [talks](https://vimeo.com/109234717) about multi-cultural dimensions of change and corporate culture.

[Dutch:](https://www.nrcq.nl/2015/06/02/zo-probeert-de-duitse-terminator-malaysia-airlines-te-redden) article about Malaysia Airlines's bankruptcy and Christoph Mueller's approach to save the company.
