---
source: TrendForum
created: '2011-09-24'
author: Jürgen
title: Seasteading
categories:
- Globalisering
- Techniek
- Maatschappij
---
[Seasteading](https://en.wikipedia.org/wiki/Seasteading) is the concept of creating permanent dwellings at sea, called seasteads, outside the territories claimed by the governments of any standing nation.
