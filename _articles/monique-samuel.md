---
source: TrendForum
created: '2011-02-12'
author: Jürgen
title: Monique Samuel
categories:
- Maatschappij
- People
---
Hier een interessante persoon:

Monique is politicologe, Christen, van Egyptische achtergrond en momenteel op de tv. Informatie over haar hier op [wikipedia](https://nl.wikipedia.org/wiki/Monique_Samuel) en haar [webpagina](https://www.moniquesamuel.nl/).

Hier: [Soeterbeeck Preeck door Monique Samuel - 20 mei 2012](https://www.youtube.com/watch?v=fY_7u8CTUGc). Zij vergelijkt de twee culturen waar ze in opgegroeid is, de Nederlandse en de Arabische. Zij eindigt met een opmerkelijk getuigenis.
