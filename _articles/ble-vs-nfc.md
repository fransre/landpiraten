---
source: TechSquare
created: '2014-01-07'
author: Jürgen
title: BLE vs NFC
categories:
- Architecture
- Devices
---
Some people [claim](https://www.ipglab.com/2013/09/21/thoughts-on-ibeacon-ble-nfc/) that [BLE](https://en.wikipedia.org/wiki/Bluetooth_low_energy) has won over [NFC](https://en.wikipedia.org/wiki/Near_field_communication) after its adoption by Apple in iOS7's [iBeacon](https://en.wikipedia.org/wiki/Ibeacon). It can be used by [Internet of Things](https://en.wikipedia.org/wiki/Internet_of_things) type of applications like personal sensors connected to one's smartphone or information beacons broadcasting to passing by smartphones. The encompassing term is [context-aware computing](https://en.wikipedia.org/wiki/Context_awareness) (see Scoble and Israel: [Age of Context: Mobile, Sensors, Data and the Future of Privacy](https://www.amazon.com/Age-Context-Mobile-Sensors-Privacy/dp/1492348430))
