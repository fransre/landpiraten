---
source: TrendForum
created: '2018-02-12'
author: Jürgen
title: NESTown
categories:
- Ontwikkelingssamenwerking
- Globalisering
---
[NESTown](https://www.nestown.org/content/vision) (short for: New Ethiopia Sustainable Town) is an association founded and domiciled in Bern, Switzerland
