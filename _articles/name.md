---
source: Enkairoi
created: '2009-01-03'
author: Jürgen
title: Name
categories:
---

**En Kairoi** - *greek for:* at the appropriate point in time

More explanation and related terms include the following:
- [Kairos](/kairos/)
- **Respondeo etsi mutabor** - a term coined by [Eugen Rosenstock-Huessy](https://en.wikipedia.org/wiki/Eugen_Rosenstock-Huessy) [in German](https://de.wikipedia.org/wiki/Eugen_Rosenstock-Huessy) [in Dutch](https://nl.wikipedia.org/wiki/Eugen_Rosenstock-Huessy) as a motto for a new scientific period succeeding the previous mottos
*credo ut intelligam* and *cogito ergo sum*.
See [Farewell to Descartes](https://theopolisinstitute.com/leithart_post/farewell-to-descartes/)
- **Tempus occasio** - which may translate to something like "seize the kairos moment" when you're creative.
