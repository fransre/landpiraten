---
source: TechSquare
created: '2014-04-24'
author: Jürgen
title: Netcat releases album as Linux kernel module
categories:
- Devices
- Human Technical-System Interfacing
---
A Seattle-based band called [netcat](https://www.netcat.co/) - not to be confused with the networking tool of the same name - has perked ears in the software community by releasing its debut album as a Linux kernel module (among other more typical formats.)

Why?

From the band's Facebook page:

"Are you ever listening to an album, and thinking 'man, this sounds good, but I wish it crossed from user-space to kernel-space more often!' We got you covered. Our album is now fully playable as a loadable Linux kernel module."
