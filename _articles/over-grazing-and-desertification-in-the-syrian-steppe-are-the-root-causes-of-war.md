---
source: TrendForum
created: '2023-07-22'
author: Frans
title: Over-grazing and desertification in the Syrian steppe are the root causes of war
categories:
- Maatschappij
---

Civil war in Syria is the result of the desertification of the ecologically fragile Syrian steppe, writes Gianluca Serra - a process that began in 1958 when the former Bedouin commons were opened up to unrestricted grazing. That led to a wider ecological, hydrological and agricultural collapse, and then to a 'rural intifada' of farmers and nomads no longer able to support themselves.

![The edge of an experimental sheep grazing exclusion zone (to the right) within Al Talila Reserve, Palmyra, photographed in March 2008 in the midst of an intense drought period. Sheep quasi uncontrolled grazing was allowed to the left of the fence. Grazing of reintroduced native antelopes at low densities had been allowed within the exclusion zone for a period of 10 years. Photo: Gianluca Serra.](https://theecologist.org/sites/default/files/styles/inline_l/public/NG_media/393957.jpg?itok=AWC3LKjT)

[Article from The Ecologist by Gianluca Serra on 2015-06-05](https://theecologist.org/2015/jun/05/over-grazing-and-desertification-syrian-steppe-are-root-causes-war)
