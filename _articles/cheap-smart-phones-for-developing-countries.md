---
source: TrendForum
created: '2011-08-17'
author: Frans
title: Cheap smart phones for developing countries
categories:
- Ontwikkelingssamenwerking
- Globalisering
- Techniek
---
![Huawei IDEOS](https://www.huaweidevice.com/resource/mini/201008174756/ideos/images/IDEOS_photo.gif)

The Chinese company [Huawei](https://www.huaweidevice.com/worldwide/) is selling [IDEOS](https://www.huaweidevice.com/resource/mini/201008174756/ideos/), [80 dollar smart phones like hotcakes in Kenya](https://singularityhub.com/2011/08/16/80-android-phone-sells-like-hotcakes-in-kenya-the-world-next/). They simply lowered the price by using less powerful hardware and equip it with a free (as in beer) open source platform: [Android](https://en.wikipedia.org/wiki/Android_(operating_system)).

And suddenly a lot of usefull apps appear: [M-Farm](https://mfarm.co.ke/) that broadcasts product prices and locations to the world via SMS, an app that diagnoses and tracks the spread of crop diseases via crowdsourcing and [Medkenya](https://www.shimbamobile.com/index.php?option=com_content&view=article&id=22:products&catid=29&Itemid=50) with a library of health information that guides the ill to hospitals.
