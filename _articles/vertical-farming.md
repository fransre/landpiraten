---
source: TrendForum
created: '2011-07-18'
author: Jürgen
title: Vertical Farming
categories:
- Globalisering
- Techniek
---
[Interesting concept](https://en.wikipedia.org/wiki/Vertical_farming) and a leading Dutch company: [PlantLab](https://www.plantlab.nl/4.0/): [Does it really stack up?](https://www.economist.com/node/17647627)

Philips opent landbouwlab ['Grow Wise'](https://www.ed.nl/economie/philips/philips-opent-landbouwlab-grow-wise-in-eindhoven-1.5060812) in Eindhoven

[THE VERTICAL FARM](https://www.newyorker.com/magazine/2017/01/09/the-vertical-farm)
Growing crops in the city, without soil or natural light.

David Rosenberg - [Vertical Farming](https://www.youtube.com/watch?v=sEqXr8cpW9g), Pitfalls and Opportunities

Ikea on [Vertical Farming](https://www.apartmenttherapy.com/grow-your-own-food-free-plans-for-ikeas-growroom-vertical-farm-242129)
