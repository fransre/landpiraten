---
source: TechSquare
created: '2011-04-01'
author: Jürgen
title: Who Gets Priority on the Web? - "Internet Architecture and Innovation"
categories:
- Baseline
- Architecture
---
Lawrence Lessig [promoting net neutrality](https://www.nytimes.com/roomfordebate/2010/8/9/who-gets-priority-on-the-web/a-deregulation-debacle-for-the-internet) by refering to a [new book](https://www.amazon.com/Internet-Architecture-Innovation-Barbara-Schewick/dp/0262013975) by Barbara van Schewick.
Here a video [presenting her book](https://www.youtube.com/watch?v=YOqHFq3h7Qg).
