---
source: Enkairoi
created: '2011-08-18'
author: Jürgen
title: Mission
categories:
---
**εν καιρῳ** ([en kairoi](/name/)) responding to today

#### 1. Our Mission

*Develop an understanding and a Christian response for the imperatives of our [time](/tijd/).*
Some examples:
- The current financial & economic crisis calls for the (re)application of "biblical" values in our financial & economic relationships.
- Global poverty calls for business relationships between people in the developed and the developing world that reflect the words of Jesus: "So whatever you wish that men would do to you, do so to them" (also known as the [Golden Rule](http://en.wikipedia.org/wiki/Ethic_of_reciprocity) or ethic of reciprocity).
through [Double Listening](/double-listening/)

#### 2. Our Vision

*Facilitate effective debates between different understandings & responses for the imperatives of our time.*

#### 3. How we work

*We scan, share and exchange information on this website and in *face-to-face meetings*.
