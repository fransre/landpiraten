---
source: TrendForum
created: '2010-12-20'
author: Jürgen
title: Adult education
categories:
- Geschiedenis
---
Eugen Rosenstock-Huessy is one of the founding fathers of adult education, which he called andragogy.
Norman Fiering wrote an essay about Rosenstock-Huessy and his role in [adragogy](https://erhsociety.com/ERHS/Essay05).
