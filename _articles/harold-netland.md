---
source: Enkairoi
created: '2009-07-26'
author: Willem
title: Harold Netland
categories:
- People
---

[Harold Netland](https://en.wikipedia.org/wiki/Harold_A._Netland) is a professor of philosophy of religion and intercultural studies at an evangelical university. Globalisation and religious pluralism are two of his areas, on which he has given a series of talks at Dallas seminary on religion and globalisation.

He makes some interesting remarks:
- In [Globalization: Some Emerging Patterns](https://voice.dts.edu/chapel/globalization-some-emerging-patterns-harold-netland/):
   - At the root of globalisation lays awareness of an increasingly complex interrelatedness on multiple levels across traditional boundaries.
   - He quotes the general introduction of [Globalization Reader](https://www.amazon.com/Globalization-Reader-Frank-J-Lechner/dp/1405102802) by Frank Lechner and John Boli, which says about improved mobility and communication: Such links are the raw material of globalization. Adding to that globalised institutions, culture and a consciousness of it.
   - He quotes Malcolm Waters' definition of globalization from [Globalization](https://www.amazon.com/Globalization-Key-Ideas-Malcolm-Waters/dp/0415238544): A social process in which the constraints of geography on economic, political, social and cultural engagements recede, in which people become increasingly aware that they are receding and in which people act accordingly.
   - Different cultures and religions are much more in contact nowadays, leading to change, which has a destabilizing effect on traditional patterns.
   - Globalisation does not eliminate local social and religious patterns, but creates fresh expression of them, and so transforms the old. As an example he mentions symbolism that can differ, like this.
   - There has been a shift to religious pluralism in the west (which he describes as the idea that the same divine reality leads to different human responses and so that all major religions are equally relevant and true), because
      - Post-christendom context which has become deeply sceptical to christian traditional claims.
      - Globalisation has brought an enlarged awareness of and contact with other religions.
      - The desire to affirm the increasing cultural diversity and to live together peacefully. It's notoriously difficult to separate strictly cultural or social issues, from religious issues. Thus for many people today acceptance of cultural diversity becomes synonymous with embracing religious diversity. Not simply in the sense of accepting legally and socially the place of non-christian religions in societies, but also in affirming the belief of such religions. And thus the very legitimate concern for social peace and harmony can result in misleading perspectives which minimise the differences between religions.
   - The [Lausanne movement](https://lausanne.org/) started in 1974 as platform a for missionaries from all over the world.
