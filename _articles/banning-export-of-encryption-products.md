---
source: TechSquare
created: '2016-02-13'
author: Jürgen
title: Banning export of encryption products
categories:
- Privacy
- Security
---
US can't ban encryption because most of it [comes from overseas}(https://www.zdnet.com/article/us-cannot-demand-encryption-backdoors-because-most-of-it-comes-from-overseas)

[A Worldwide Survey of Encryption Products](https://www.schneier.com/cryptography/archives/2016/02/a_worldwide_survey_o.html)
