---
source: TrendForum
created: '2015-05-10'
author: Jürgen
title: 'Winkler: History of the West'
categories:
- Geschiedenis
---
[Heinrich August Winkler](https://en.wikipedia.org/wiki/Heinrich_August_Winkler)'s lecture at the LSE:
[Greatness and Limits of the West - The History of an Unfinished Project](https://www.lse.ac.uk/europeanInstitute/LEQS/LEQSPaper30.pdf)

He just finished his opus magnus:
Geschichte des Westens

1. [Von den Anfängen in der Antike bis zum 20. Jahrhundert](https://www.amazon.de/Geschichte-Westens-Anf%C3%A4ngen-Antike-Jahrhundert/dp/340659235X)
2. [Die Zeit der Weltkriege 1914-1945](https://www.amazon.de/Geschichte-Westens-Zeit-Weltkriege-1914-1945/dp/3406592368)
3. [Vom Kalten Krieg zum Mauerfall](https://www.amazon.de/Geschichte-Westens-Kalten-Krieg-Mauerfall/dp/3406669840)
4. [Die Zeit der Gegenwart](https://www.amazon.de/Geschichte-Westens-Die-Zeit-Gegenwart/dp/3406669867)
