---
source: TrendForum
created: '2016-05-03'
author: Jürgen
title: The Service Year Alliance
categories:
- Ontwikkelingssamenwerking
- Globalisering
- Maatschappij
---
The Franklin Project, ServiceNation, & Service Year Exchange Merger:

- We are thrilled to share with you that the Franklin Project, ServiceNation and the Service Year Exchange (incubated by the National Conference on Citizenship) have merged into a new, single organization. The organization will be a joint venture between Be The Change, Inc. and The Aspen Institute, with Stan McChrystal serving as Chair of the board.
- We’ve merged because the stakes are too high to continue working individually.
- This new organization will be known as the Service Year Alliance in the short term...

The [Franklin Project](https://www.franklinproject.org/): Making a year of national service a cultural expectation, common opportunity, and civic rite of passage for every young American

