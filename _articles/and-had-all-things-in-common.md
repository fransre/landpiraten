---
source: TrendForum
created: '2011-04-04'
author: Jürgen
title: and had all things in common
categories:
- Geschiedenis
- Kerk
- Maatschappij
---
[Book, interview](https://amishamerica.com/i-am-hutterite-5-book-giveaway-and-interview-with-mary-ann-kirkby/) and [film](https://amishamerica.com/the-hutterites/) about Hutterite living. A community different from the world.
