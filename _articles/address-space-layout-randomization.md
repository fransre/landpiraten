---
source: TechSquare
created: '2011-03-11'
author: Jürgen
title: address space layout randomization
categories:
- Baseline
- Devices
---
ASLR makes it makes it hard to hack [mobile phones](https://www.theregister.co.uk/2011/03/11/iphone_blackberry_hacked/).
