---
source: TrendForum
created: '2012-12-23'
author: Jürgen
title: Tennis as a development aid
categories:
- Ontwikkelingssamenwerking
---
[Tariku and Desta Kids’ Education through Tennis Development](https://tdket.org/)
has the goal to help disadvantaged children escape poverty. The objectives for the children of TDKET-Ethiopia are:

* To seek a high school education with the goal of attending college or university.
* To experience the world of tennis and explore the limits of their talent.
* To provide an example to other children of the value of hard work and hope.
