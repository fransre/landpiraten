---
source: TrendForum
created: '2014-09-21'
author: Jürgen
title: 'Communities in Bloom - 2014: Weert'
categories:
- Globalisering
- Maatschappij
---
Gemeente Weert is the [winner](https://us8.campaign-archive1.com/?u=c0075f96bfa1a5bcd571e91b0&id=e37ba51409&e=a7e299c28c) of the International Challenge (Medium) category. Weert received a 5 Bloom rating and a special mention for Kempen-Broek Cross Border Park during the 2014 Symposium and Awards Ceremonies in Charlottetown, PEI.
