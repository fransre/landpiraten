---
source: TechSquare
created: '2011-03-25'
author: Jürgen
title: Away from 3D
categories:
- Devices
- Human Technical-System Interfacing
---
["We're moving away from any stance that says if you don't use the 3-D functionality you can't play this game."](https://arstechnica.com/gaming/news/2011/03/nintendo-backs-away-from-3ds-games-that-require-3d-others-may-follow.ars)
→ not everyone can see the 3D images
