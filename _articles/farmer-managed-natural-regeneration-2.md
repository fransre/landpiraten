---
source: TrendForum
created: '2019-03-29'
author: Jürgen
title: Farmer-managed natural regeneration
categories:
- Ontwikkelingssamenwerking
---
[Farmer-managed natural regeneration](https://en.wikipedia.org/wiki/Farmer-managed_natural_regeneration) (FMNR) is a low-cost, sustainable land restoration technique used to combat poverty and hunger amongst poor subsistence farmers in developing countries by increasing food and timber production, and resilience to climate extremes. It involves the systematic regeneration and management of trees and shrubs from tree stumps, roots and seeds.

A masterclass [Reforesting 5 Million+ Hectares in the Desert Tony Rinaudo's FMNR, created in 2017](https://www.youtube.com/watch?v=ajfc5jdgWXc)

For a broader discussion see: [Africa innovations: 15 ideas helping to transform a continent](https://www.theguardian.com/world/2012/aug/26/africa-innovations-transform-continent)
