---
source: TrendForum
created: '2011-01-20'
author: Jürgen
title: Papers of Eugen Rosenstock-Huessy and Franz Rosenzweig Donated to Dartmouth
categories:
- Geschiedenis
- Kerk
---
[Gritli letters and other correspondence](https://now.dartmouth.edu/2011/01/papers-of-eugen-rosenstock-huessy-and-franz-rosenzweig-donated-to-dartmouth/) moved to Dartmouth.
