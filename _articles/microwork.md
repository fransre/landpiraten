---
source: TrendForum
created: '2013-04-13'
author: Jürgen
title: Microwork
categories:
- Globalisering
- Management
- Financiën
---
[Microwork](https://en.wikipedia.org/wiki/Microwork) is a series of small tasks which together comprise a large unified project, and are completed by many people over the Internet. Microwork is considered the smallest unit of work in a virtual assembly line.

Next to [Amazon Mechanical Turk](https://en.wikipedia.org/wiki/Amazon_Mechanical_Turk), Samasource is a non-profit organization.

[Samasource](https://samasource.org/company/) delivers enterprise digital services through a unique Microwork™ model that harnesses the untapped potential of the world’s poor.

Samasource was founded in 2008 by [Leila Janah](https://leilajanah.com/talks/) a social entrepreneur interested in using technology and new business methods to reduce poverty and promote social justice.

[Leila Janah: End Poverty: Give Work](https://www.youtube.com/watch?v=-i2ddkCHqZY) compares Fair-Trade business volume to microwork transactions for people from the bottom of the pyramid.
