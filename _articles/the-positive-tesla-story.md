---
source: TechSquare
created: '2016-08-20'
author: Jürgen
title: The Positive Tesla Story
categories:
- Devices
- Human Technical-System Interfacing
---
How Tesla Autopilot drove a man with a blood clot [to the hospital](https://www.techrepublic.com/article/how-tesla-autopilot-drove-a-man-with-a-blood-clot-to-the-hospital-and-expanded-the-autonomous-car), and expanded the autonomous car debate.
