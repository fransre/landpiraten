---
source: TrendForum
created: '2010-12-21'
author: Jürgen
title: George Soros - The Living History of the Last 30 years
categories:
- Geschiedenis
- Globalisering
- Wetenschap
- People
---
George Soros has founded the Institute for New Economic Thinking to support the change of the economics discipline.

[George Soros - The Living History of the Last 30 years](https://www.youtube.com/watch?v=XVC9mwQqWIY)

Soros gives an account of the history of economic concepts during (part of) the 20th century.

[The YT channel of INET](https://www.youtube.com/user/INETeconomics)
