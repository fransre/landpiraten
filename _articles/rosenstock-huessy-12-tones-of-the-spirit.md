---
source: EensVoorAl
created: '2024-07-07'
author: Jürgen
title: Rosenstock-Huessy's 12 Tones of the Spirit
---
the Spirit reverses this order of naturalism:
- In nature, birth precedes death;
- In nature, life tries to shun death.
- In the spirit, death precedes life;
- In the spirit, the founder's death guides his heirs' lives.



##### I. Youth or childhood; in society represented by the artist.
- 12\. obey - existential - involved
- 11\. read - mental - detached
- 10\. learn - selective - half-involved, half-detached
- 9\. sing all this time, as your hour has not yet come

##### II. Adulthood; in society represented by the fighter.
- 8\. doubt and withhold - half-involved, half-detached
- 7\. analyze and synthesize - detached
- 6\. speak up and insist - get involved
- 5\. wait and persevere - stay engaged

##### III. The Elders; the universal priesthood of the believers.
- 4\. lead and legislate
- 3\. teach and instruct
- 2\. prophesy and warn
- 1\. "testate", endow, and bestow

taken from [I am an impure thinker](https://www.erhfund.org/wp-content/uploads/588.pdf)

see also: [Lehre Rosenstock-Huessy's / Rosenstock-Huessys leer]({{ 'lehre-rosenstock-huessys-rosenstock-huessys-leer' | relative_url }})
