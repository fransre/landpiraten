---
source: TrendForum
created: '2012-03-21'
author: Jürgen
title: Are Women Better Leaders than Men?
categories:
- Management
---
HBR [Survey](https://blogs.hbr.org/cs/2012/03/a_study_in_leadership_women_do.html) about women in leadership

The story of a specific woman: [Sheryl Sandberg](https://www.newyorker.com/reporting/2011/07/11/110711fa_fact_auletta?currentPage=all)

[Petra Stienen](https://nl.wikipedia.org/wiki/Petra_Stienen) tells about the [Inspiration of the Seven Sisters](https://www.youtube.com/watch?v=ih8pQyR-T1c), women she got to know in/from the Arabic countries.
Over haar [persoonlijke](https://www.intermediair.nl/artikel/59825/petra-stienen-er-wordt-vaak-gedacht-dat-diplomaten-luxebeestjes-zijn.html) achtergrond en haar werk

[Makers](https://www.makers.com/) - Women Who Make America: a video platform with stories from women who made a difference.

[Women in the World](https://www.thedailybeast.com/witw.html): yearly conference and website for equal rights and opportunity.
