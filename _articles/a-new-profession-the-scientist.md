---
source: TrendForum
created: '2013-05-03'
author: Willem
title: 'A new profession: the scientist'
categories:
- Geschiedenis
- Wetenschap
- Maatschappij
---
"Scientist" is one of those words that haven't been around for so long - 180 years to be exact. Natural philosophers was the previous term. In her [TED talk](https://www.ted.com/talks/laura_snyder_the_philosophical_breakfast_club.html) Laura Snyder critiques the current scientific community, saying "science is not only for scientists". She wrote on a book on this history: [The Philosophical Breakfast Club: Four Remarkable Friends who Transformed Science and Changed the World](https://laurajsnyder.com/philosophical-breakfast-club/).
