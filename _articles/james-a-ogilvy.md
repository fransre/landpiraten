---
source: Enkairoi
created: '2009-02-15'
author: Jürgen
title: James A. Ogilvy
categories:
- People
---

in progress

see a [bio](https://www.triarchypress.net/james-jay-ogilvy.html) of James A. Ogilvy
Jay is a cofounder of GBN and a partner of the Monitor Group. His research and work focus on the role that human values and changing motivations play in business decision-making and strategy. He has pursued these interests in collaboration with Peter Schwartz since 1979, when he joined SRI International, and since 1988 with GBN. While at SRI, Jay split his time between developing future scenarios for strategic planning and serving as director of research for the Values and Lifestyles (VALS) Program, a consumer segmentation system used in market research. He also authored monographs on social, political, and demographic trends affecting the values of American consumers.

Jay's work builds on his background as a philosopher. He taught at the University of Texas, Williams College, and for seven years at Yale, where he received his PhD in 1968. He is the author of Creating Better Futures: Scenario Planning as a Tool for a Better Tomorrow (2002), Living Without a Goal (1995), Many Dimensional Man (1977); co-author of China' Futures (2001) and Seven Tomorrows (1980); and editor of Self and World (1971, 1980) and Revisioning Philosophy (1991).

Now Ogilvy shows how we can use this cutting-edge method for social change in our own neighborhoods.

## James A. Ogilvy: Creating Better Futures

In [Creating Better Futures](https://www.amazon.com/Creating-Better-Futures-Scenario-Planning/dp/0195146115), Ogilvy presents a profound new vision of how the world is changing--and how it can be changed for the better. Ogilvy argues that self-defined communities, rather than individuals or governments, have become the primary agents for social change. Towns, professional associations, and interest groups of all kinds help shape the future in all the ways that matter most, from schools and hospitals to urban development. The key to improvement is scenario planning--a process that draws on groups of people, both lay and expert, to draft narratives that spell out possible futures, some to avoid, some inspiring hope. Scenario planning has revolutionized both public and private planning, leading to everything from the diverse product lines that have revived the auto industry, to a timely decision by the state of Colorado to avoid pouring millions into an oil-shale industry that never materialized. But never before has anyone proposed that it be taken up by society as a whole. Drawing on years of experience in both academia and the private sector, where he developed both a keen sense of how businesses work best and an abiding passion for changing the world, James Ogilvy provides the tools we need to create better communities: better health, better education, better lives.

### Table of Contents:
- Foreword by Peter Schwartz
- Preface

### ONE: Aiming Higher
- Creating the futures we want
- From the old paradigm to a new worldview
- Values in a pluralistic world
- Scenario Planning
- Plan of the book

#### Part One: New Game
### TWO: Religious, Political, and Economic Passions
- This postmodern business
- From the idea of progress to historical drift
- From the rise of science to post-scientific inquiry
- From industrial mass manufacturing to the knowledge economy
- Import for organizational design
- Beyond the nation-state: from the political era to the economic era

### THREE: The Limits of the Marketplace
- What markets can not do
- Funding public education
- The debate over educational standards
- Who is the chooser of health care?
- What are our choices in telecommunications?

### FOUR: Rethinking Representative Government
- Strategic planning and industrial policy
- The likeness between learning and evolution
- Rethinking representation

#### Part TWO: New Players
### FIVE: Beyond Individualism and Collectivism
- Reframing the conflict between individualists and collectivists
- In favor of individualism
- Too much of a good thing?
- The shift of worldviews in psychology

### SIX: Social Forces and Creativity
- Jürgen Habermas and reflexive sociology
- Marxism's problem
- Communities as creative agents
- Social creativity
- Communal creativity in practice

#### Part THREE: New Lenses
### SEVEN: From Worldviews To Better Worlds
- Kuhn's contribution
- Kuhn's contribution in context
- What paradigms are like
- Anthropology: from explanation by law to the interpretation of meaning
- Import for scenario planning
- Literary criticism and the legacy of existentialism
- The import of recent literary criticism for scenario planning

### EIGHT: Features Of The Relational Worldview
- From things to symbols: the semiotic turn
- Difference over identity: from an emphasis on things to an emphasis on relationships
- From explanation to narration: the importance of stories
- The fall into time
- The democratization of meaning
- Ambiguity
- Heterarchy
- Applying the new tools to the new rules

#### Part FOUR: New Rules, New Tools
### NINE: Facts, Values, And Scenario Planning
- A new look at norms
- A fresh look at values
- The developmental model of maturation
- A concrete description of values in everyday life
- Theories of value
- Summary and synthesis
- From the moral majority to moral maturity

### TEN: Scenario Planning: A Tool For Social Creativity

#### Part FIVE: Scenario Planning in Action
### ELEVEN: Better Education
- Standards and accountability
- Privatization
- Technology
- School finance
- School governance
- The public agenda
- Demographics
- The technology fix
- The corporate fix: privatization

### TWELVE: Better Health Care
- Better health care
- Defined contribution
- Relationship-centered care

### THIRTEEN: Cadenza: Earth Might Be Fair
- Why there can not be one best future
