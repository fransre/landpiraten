---
source: TechSquare
created: '2014-03-06'
author: Jürgen
title: Advice from Ballmer
categories:
- Policy
---
[Interview](https://www.youtube.com/watch?v=amc-rwsQXrA)

- don't fail fast
- take a long term view
- "Microsoft" means affordable, empowering Software
