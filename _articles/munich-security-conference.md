---
source: TrendForum
created: '2014-02-01'
author: Jürgen
title: Munich Security Conference
categories: []
---
Over the past five decades the Munich Security Conference ([MSC](https://www.securityconference.de/en/media-library/)) has become a key annual gathering for the international "strategic community."

There is a big media library on the website.

The Transatlantic Trade and Investment Partnership ([TTIP](https://ec.europa.eu/trade/policy/in-focus/ttip/#what_is_ttip)) is a trade agreement that is presently being negotiated between the European Union and the United States.

"The Belgrade-Pristina Dialogue" @[MSC](https://www.securityconference.de/en/media-library/)
Panel Discussion: Ivica Dačić (Prime Minister, Belgrade), Hashim Thaçi (Prime Minister, Pristina), Baroness Catherine Ashton (Vice President of the European Commission; High Representative for Foreign Affairs and Security Policy, European Union, Brussels)

Confessionals of the two prime ministers about the most difficult times of the last 15 month of negotiations which were guided by Ashton. Just amazing!
