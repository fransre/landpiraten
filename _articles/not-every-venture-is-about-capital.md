---
source: TrendForum
created: '2011-04-04'
author: Willem
title: '"Not every venture is about capital"'
categories:
- Techniek
- Maatschappij
---
![Doing good is part of our code](https://farm6.static.flickr.com/5108/5569324504_b5a30e8dfc_z.jpg)

[and more](https://www.flickr.com/photos/fligtar/5568739221/in/set-72157626249531015): Mozilla communicating some of its ethos.
