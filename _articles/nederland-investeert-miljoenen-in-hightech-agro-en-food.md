---
source: TrendForum
created: '2016-11-04'
author: Jürgen
lang: nl
title: Nederland investeert miljoenen in hightech agro en food
categories:
- Globalisering
- Techniek
- Maatschappij
---
Tijdens de opening van Dutch Agri Food Week maakte staatssecretaris Van Dam van Economische Zaken bekend acht miljoen euro beschikbaar te stellen voor hightech innovaties in de landbouw en voedselindustrie. De innovaties moeten ervoor zorgen dat de sectoren duurzamer kunnen produceren en ook in de toekomst toonaangevend blijven. Naar verwachting investeert het bedrijfsleven een bedrag van vergelijkbare omvang. De staatssecretaris kondigde ook aan dat er nog eens een half miljoen beschikbaar komt voor start-upversnellers. Begin volgend jaar start de eerste accelerator voor tuinbouw. Daarna volgen versnellers voor food en agrarische technologie.

Voor het innovatiepotje selecteren de topsectoren [HTSM, Agri & Food en Tuinbouw & Uitgangsmaterialen](https://www.bits-chips.nl/artikel/nederland-investeert-miljoenen-in-hightech-agro-en-food-47993.html) projecten die technologie uit de verschillende expertises combineren. Het kan gaan om landbouwrobots die 24/7 op een milieuvriendelijke manier het land bewerken, big data om antibioticagebruik terug te dringen en energie te besparen, intelligente sensoren die bestaande landbouwsystemen superefficiënt maken of satellietbeelden en sensoren om precies te zien waar water, mest of gewasbescherming nodig is. De gehonoreerde innovatieprojecten en de omvang van de investering onthullen de topsectoren binnen enkele weken.

‘De Nederlandse landbouw staat internationaal bekend als innovatief en efficiënt. Met de toepassing van de nieuwste innovaties uit de hightech en ict kunnen we die positie verder versterken’, aldus staatssecretaris Van Dam. ‘Dat draagt bij aan de wereldwijde opgave om genoeg eten te produceren op een duurzame manier, en biedt ook kansen voor onze export.’

[How The Netherlands Became The World's Second Largest Agricultural Exporter](https://www.youtube.com/watch?v=kPPG98yw7-k)
f somebody asked you to name the top three countries for agricultural export, the Netherlands probably wouldn’t feature. But, in actuality, the Dutch have masterfully played the exportation game to their advantage, and, in doing so, have become the second largest agricultural exporter in the world. In the agricultural sector, the Dutch are predominantly known for producing high-quality goods (in terms of vegetables – tomatoes and chillies, dairy – cheese and milk, in addition to numerous kinds of flowers and plants.) Therefore, they’re not only voluminous in terms of how much they physically shift, but also in the quality of their products. So, how did the Netherlands manage to conquer the market and take over? As the Dutch themselves would say, laten we leren!
